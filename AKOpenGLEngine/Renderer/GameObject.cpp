//
//  GameObject.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>
#include <iostream>

#include "GameObject.h"
#include "Components/Camera.h"
#include "Components/Transform.h"
#include "Components/MeshRenderer.h"
#include "Components/Components.h"
#include "Components/Light.h"
#include "Managers/ReferenceManager.h"

using namespace std;
using namespace YAML;

namespace AKOpenGLEngine {

    void GameObject::Update(float dt) {
        if (!isEnabled)
            return;
        for (auto it = components.begin(); it < components.end(); ++it) {
            (*it)->Update(dt);
        }
    }

    void GameObject::FixedUpdate(float dt) {
        if (!isEnabled)
            return;
        for (auto it = components.begin(); it < components.end(); ++it) {
            (*it)->FixedUpdate(dt);
        }
    }

    Transform *GameObject::GetTransform() const {
        return transform;
    }

    GameObject::GameObject() {
        isEnabled = true;
        name = "GameObject";
        transform = new Transform();
        AddComponent(transform);
    }

    GameObject::GameObject(Node gameObjectNode) {
        new(this) GameObject();

        Node componentsNode = gameObjectNode["components"];
        for (const_iterator componentsIterator = componentsNode.begin(); componentsIterator != componentsNode.end(); ++componentsIterator) {
            Component *component = Components::getInstance().GetComponent(*componentsIterator);
            AddComponent(component);
            ReferenceManager::getInstance().RegisterReference((*componentsIterator)["id"].as<long long int>(), component);
        }
    }

    void GameObject::RemoveComponent(Component *component) {
        for (auto it = components.begin(); it < components.end(); ++it) {
            if ((*it) == component) {
                components.erase(it);
                delete (*it);
                break;
            }
        }
    }

    GameObject *GameObject::CreateCamera() {
        GameObject *go = new GameObject();
        go->AddComponent(new Camera());
        go->SetName("Camera");

        return go;
    }

    void GameObject::AddComponent(Component *component) {
        if(component == nullptr) {
            return;
        }
        if(component->GetName() == "Transform") {
            auto it = find_if(components.begin(), components.end(), [](const Component* c) {return c->GetName() == "Transform";});
            if(it != components.end()) {
                components.erase(it);
            }
            transform = (Transform*)component;
        }
        components.push_back(component);
        component->AssignGameObject(this);
    }

    void GameObject::Render(unsigned int renderPass, Camera *camera, bool useLight) {
        for(auto component : components) {
            component->Render(renderPass, camera, useLight);
        }
    }

    bool GameObject::HasComponentOfType(std::string type) const {
        for (auto component : components) {
            if (component->GetName() == type) {
                return true;
            }
        }
        return false;
    }

    Component* GameObject::GetComponentOfType(std::string type) const {
        for (auto component : components) {
            if (component->GetName() == type) {
                return component;
            }
        }
        return nullptr;
    }

    std::string GameObject::GetName() const {
        return name;
    }

    void GameObject::SetName(std::string name) {
        this->name = name;
    }

    bool GameObject::operator<(GameObject &obj) {

        MeshRenderer *c1 = (MeshRenderer*)GetComponentOfType("MeshRenderer");
        MeshRenderer *c2 = (MeshRenderer*)obj.GetComponentOfType("MeshRenderer");
        uintptr_t p1, p2;
        if(c1 == nullptr) {
            p1 = 0;
        } else {
            p1 = (uintptr_t)c1->GetMaterial();
        }
        if(c2 == nullptr) {
            p2 = 0;
        } else {
            p2 = (uintptr_t)c2->GetMaterial();
        }return p1<p2;
    }

    vector<Component *> GameObject::GetComponents() const {
        return components;
    }

    Emitter &operator<<(Emitter &out, const GameObject *e) {
        out << BeginMap;
        out << Key << "name" << Value << e->GetName();
        out << Key << "id" << Value << (long long int) e;
        out << Key << "components" << Value << e->GetComponents();
        out << EndMap;
        return out;
    }

    GameObject *GameObject::CreateDirectionalLight() {
        GameObject *go = new GameObject();
        go->AddComponent(new Light());
        go->SetName("Light");
        return go;
    }

    GameObject *GameObject::CreatePointLight() {
        GameObject *go = new GameObject();
        Light *light = new Light();
        light->setLightType(LightType::Point);
        go->AddComponent(light);
        go->SetName("Light");
        return go;
    }

    GameObject *GameObject::CreateSpotLight() {
        GameObject *go = new GameObject();
        Light *light = new Light();
        light->setLightType(LightType::Spot);
        light->setLightSpotAngle((float)M_PI_4);
        go->AddComponent(light);
        go->SetName("Light");
        return go;
    }

}