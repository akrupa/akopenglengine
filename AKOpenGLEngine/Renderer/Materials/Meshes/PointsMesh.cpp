//
//  PointsMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "PointsMesh.h"
#include "Point.h"

using namespace std;

namespace AKOpenGLEngine {

    PointsMesh::PointsMesh() : Mesh() {
        points = vector<Point>();
    }

    PointsMesh::PointsMesh(vector<Vertex> points) : Mesh() {
        this->points = vector<Point>(points.size());
        Point p;
        for(size_t i=0; i < points.size(); ++i) {
            p.indices.a = (unsigned int)i;
            this->points.push_back(p);
        }
        this->vertices = points;

    }

    unsigned int* PointsMesh::GetIndexData() {
        return (unsigned int*)points.data();
    }

    unsigned int PointsMesh::GetIndexDataSize() {
        return (unsigned int)(points.size() * sizeof(Point));
    }

    unsigned int PointsMesh::GetIndexDrawSize() {
        return (unsigned int)points.size();
    }

    Vertex* PointsMesh::GetVertexData() {
        return vertices.data();
    }

    unsigned int PointsMesh::GetVertexDataSize() {
        return (unsigned int)(vertices.size() * sizeof(Vertex));
    }

    GLenum PointsMesh::GetDrawType() {
        return GL_POINTS;
    }

}