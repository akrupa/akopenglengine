//
//  TorusMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "TorusMesh.h"
#include "../Triangle.h"

using namespace glm;

namespace AKOpenGLEngine {

    TorusMesh::TorusMesh(float R, float r, unsigned int density, vec4 const& color) : TrianglesMesh() {
        name = "TorusMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = color;

        for (unsigned int i = 0; i <= density; ++i) {
            for (unsigned int j = 0; j <= density; ++j) {
                float alpha = j * (float) M_PI * 2 / density;
                float beta = i * (float) M_PI * 2 / density;
                v.position = vec3(
                        (R + r * cos(alpha)) * cos(beta),
                        r * sin(alpha),
                        (R + r * cos(alpha)) * sin(beta));
                v.UV = vec2((float) j / (float) density, (float) i / (float) density);

                vec3 dalpha = vec3(-r * sin(alpha) * cos(beta),
                        r * cos(alpha),
                        -r * sin(alpha) * sin(beta));
                vec3 dbeta = vec3(-R * sin(beta),
                        0,
                        R * cos(beta));
                v.normal = normalize(cross(dalpha, dbeta));
                v.tangent = dalpha;
                vertices.push_back(v);
            }
        }

        Triangle t;
        for (unsigned int i = 0; i < density; ++i) {
            for (unsigned int j = 0; j < density; ++j) {

                t.indices.a = i + j*(density+1);
                t.indices.b = i + j*(density+1)+1;
                t.indices.c = i +(j+1)*(density+1);
                triangles.push_back(t);

                t.indices.a = i +j*(density+1)+1;
                t.indices.b = i + (j+1)*(density+1)+1;
                t.indices.c = i +(j+1)*(density+1);
                triangles.push_back(t);

            }
        }
    }

    TorusMesh::TorusMesh(std::map<std::string, float> parameters) {

    	float R = parameters.find("R")->second;
    	float r = parameters.find("r")->second;
    	unsigned int density = (unsigned int)parameters.find("density")->second;
    	vec4 color;
    	color.x = parameters.find("colorX")->second;
    	color.y = parameters.find("colorY")->second;
    	color.z = parameters.find("colorZ")->second;
    	color.w = parameters.find("colorW")->second;

    	new(this) TorusMesh(R, r, density, color);
    }
}
