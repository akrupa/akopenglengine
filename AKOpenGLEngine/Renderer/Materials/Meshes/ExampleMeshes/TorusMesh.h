//
//  TorusMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TorusMesh_H_
#define __TorusMesh_H_

#include <map>
#include <string>

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {

    class TorusMesh : public TrianglesMesh {

    public:
        TorusMesh(float R = 1.0f, float r = 0.2f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
        TorusMesh(std::map<std::string, float> parameters);
    };
}

#endif //__TorusMesh_H_
