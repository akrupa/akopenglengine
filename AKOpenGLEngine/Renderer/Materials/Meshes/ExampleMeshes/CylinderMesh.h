//
//  CylinderMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __CylinderMesh_H_
#define __CylinderMesh_H_

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {

    class CylinderMesh : public TrianglesMesh {

    public:
        CylinderMesh(float R = 1.0f, float height = 2, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    };
}

#endif //__CylinderMesh_H_
