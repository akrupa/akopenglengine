//
//  PointMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/glm.hpp>

#include "PointMesh.h"
#include "../Point.h"

using namespace glm;

namespace AKOpenGLEngine {

    PointMesh::PointMesh() : PointsMesh() {
        name = "PointMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        v.position = vec3(0,0,0);
        Point p;
        p.indices.a = 0;
        vertices.push_back(v);
        points.push_back(p);
    }
}