//
//  CubePointsMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __CubePointsMesh_H_
#define __CubePointsMesh_H_

#include "../PointsMesh.h"

namespace AKOpenGLEngine {

    class CubePointsMesh : public PointsMesh {

    public:
        CubePointsMesh(int pointsNumber = 5);
    };
}


#endif //__CubePointsMesh_H_
