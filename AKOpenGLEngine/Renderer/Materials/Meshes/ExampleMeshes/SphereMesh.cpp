//
//  SphereMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "SphereMesh.h"
#include "../Triangle.h"

using namespace glm;
using namespace std;

namespace AKOpenGLEngine {


    SphereMesh::SphereMesh(unsigned int density, vec4 const& color) {
        name = "SphereMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = color;

        for (unsigned int i = 0; i <= density; ++i) {
            for (unsigned int j = 0; j <= density; ++j) {
                float alpha = j * (float) M_PI * 2 / density;
                float beta = i * (float) M_PI / density;
                v.position = vec3(
                        (cos(alpha)) * sin(beta),
                        cos(beta),
                        (sin(alpha)) * sin(beta));
                v.UV = vec2((float) j / (float) density, (float) i / (float) density);

                vec3 dalpha = vec3(sin(alpha) * sin(beta),
                        0,
                        cos(alpha) * sin(beta));
                vec3 dbeta = vec3(cos(alpha) * cos(beta),
                        -sin(beta),
                        sin(alpha) * cos(beta));
                if (i == 0) {
                    v.normal = vec3(0, 1, 0);
                } else if (i == density) {
                    v.normal = vec3(0, -1, 0);
                } else {
                    v.normal = normalize(cross(dalpha, dbeta));
                }

                vertices.push_back(v);
            }
        }

        Triangle t;
        for (unsigned int i = 0; i < density; ++i) {
            for (unsigned int j = 0; j < density; ++j) {

                t.indices.a = i + j*(density+1);
                t.indices.b = i + j*(density+1)+1;
                t.indices.c = i +(j+1)*(density+1);
                triangles.push_back(t);

                t.indices.a = i +j*(density+1)+1;
                t.indices.b = i + (j+1)*(density+1)+1;
                t.indices.c = i +(j+1)*(density+1);
                triangles.push_back(t);

            }
        }
    }

    SphereMesh::SphereMesh(map<string, float> const& parameters) {
    	int density = (int)parameters.find("density")->second;
    	vec4 color;
    	color.x = parameters.find("colorX")->second;
    	color.y = parameters.find("colorY")->second;
    	color.z = parameters.find("colorZ")->second;
    	color.w = parameters.find("colorW")->second;

    	new(this) SphereMesh(density, color);
    }
}
