//
//  CubeMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 27.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __CubeMesh_H_
#define __CubeMesh_H_

#include <string>
#include <map>

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {

    class CubeMesh : public TrianglesMesh {

    public:
        CubeMesh(glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
        CubeMesh(std::map<std::string, float> const& parameters);
    };
}


#endif //__CubeMesh_H_
