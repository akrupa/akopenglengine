//
//  Meshes.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 07.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>

#include "Meshes.h"
#include "PointMesh.h"
#include "CubePointsMesh.h"
#include "LineMesh.h"
#include "CubeLinesMesh.h"
#include "CubeMesh.h"
#include "TorusMesh.h"
#include "SphereMesh.h"
#include "CylinderMesh.h"
#include "ConeMesh.h"
#include "TubeMesh.h"
#include "../../../Managers/ModelManager.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {

	Mesh *Meshes::GetMesh(std::string const& meshName) const {
		return GetMesh(meshName, map<string, float>());
	}

	Mesh *Meshes::GetMesh(string const& meshName, map<string, float> const& parameters) const {
		auto it = meshesMap.find(meshName);
		if (it != meshesMap.end()) {
			return it->second;
		}
		else {
			Mesh *mesh = nullptr;

			if (meshName == "PointMesh") {
				mesh = new PointMesh();
			}
			else if (meshName == "CubePointsMesh") {
				mesh = new CubePointsMesh();
			}
			else if (meshName == "LineMesh") {
				mesh = new LineMesh();
			}
			else if (meshName == "CubeLinesMesh") {
				mesh = new CubeLinesMesh();
			}
			else if (meshName == "CubeMesh") {
				mesh = new CubeMesh(parameters);
			}
			else if (meshName == "TorusMesh") {
				mesh = new TorusMesh(parameters);
			}
			else if (meshName == "SphereMesh") {
				mesh = new SphereMesh(parameters);
			}
			else if (meshName == "CylinderMesh") {
				mesh = new CylinderMesh();
			}
			else if (meshName == "ConeMesh") {
				mesh = new ConeMesh();
			}
			else if (meshName == "TubeMesh") {
				mesh = new TubeMesh();
			}

			if (mesh == nullptr) {
				mesh = ModelManager::LoadMesh(meshName);
			}

			if (mesh != nullptr) {
				meshesMap.insert(std::pair<string, Mesh *>(meshName, mesh));
			}
			else {
				LOG(ERROR) << "Couldn't find mesh " << meshName;
			}
			return mesh;
		}
	}

	Mesh *Meshes::GetPointMesh() const {
		return GetMesh("PointMesh");
	}

	Mesh *Meshes::GetCubePointsMesh(int pointsNumber) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("pointsNumber", pointsNumber));
		return GetMesh("CubePointsMesh", parameters);
	}

	Mesh *Meshes::GetLineMesh() const {
		return GetMesh("LineMesh");
	}

	Mesh *Meshes::GetCubeLinesMesh() const {
		return GetMesh("CubeLinesMesh");
	}

	Mesh *Meshes::GetCubeMesh(vec4 const& color) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("colorX", color.x));
		parameters.insert(pair<string, float>("colorY", color.y));
		parameters.insert(pair<string, float>("colorZ", color.z));
		parameters.insert(pair<string, float>("colorW", color.w));
		return GetMesh("CubeMesh", parameters);
	}

	Mesh *Meshes::GetTorusMesh(float R, float r, unsigned int density, vec4 const& color) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("colorX", color.x));
		parameters.insert(pair<string, float>("colorY", color.y));
		parameters.insert(pair<string, float>("colorZ", color.z));
		parameters.insert(pair<string, float>("colorW", color.w));
		parameters.insert(pair<string, float>("R", R));
		parameters.insert(pair<string, float>("r", r));
		parameters.insert(pair<string, float>("density", density));
		return GetMesh("TorusMesh", parameters);
	}

	Mesh *Meshes::GetSphereMesh(unsigned int density, vec4 const& color) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("colorX", color.x));
		parameters.insert(pair<string, float>("colorY", color.y));
		parameters.insert(pair<string, float>("colorZ", color.z));
		parameters.insert(pair<string, float>("colorW", color.w));
		parameters.insert(pair<string, float>("density", density));
		return GetMesh("SphereMesh", parameters);
	}

	Mesh *Meshes::GetConeMesh(float R, float height, float chop, unsigned int density, vec4 const& color) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("colorX", color.x));
		parameters.insert(pair<string, float>("colorY", color.y));
		parameters.insert(pair<string, float>("colorZ", color.z));
		parameters.insert(pair<string, float>("colorW", color.w));
		parameters.insert(pair<string, float>("R", R));
		parameters.insert(pair<string, float>("density", density));
		parameters.insert(pair<string, float>("height", height));
		parameters.insert(pair<string, float>("chop", chop));
		return GetMesh("ConeMesh", parameters);
	}

	Mesh *Meshes::GetTubeMesh(float R, float r, float height, unsigned int density, vec4 const& color) const {
		map<string, float> parameters;
		parameters.insert(pair<string, float>("colorX", color.x));
		parameters.insert(pair<string, float>("colorY", color.y));
		parameters.insert(pair<string, float>("colorZ", color.z));
		parameters.insert(pair<string, float>("colorW", color.w));
		parameters.insert(pair<string, float>("R", R));
		parameters.insert(pair<string, float>("r", r));
		parameters.insert(pair<string, float>("density", density));
		parameters.insert(pair<string, float>("height", height));
		return GetMesh("TubeMesh", parameters);
	}
}
