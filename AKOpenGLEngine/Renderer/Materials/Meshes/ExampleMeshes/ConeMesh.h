//
//  ConeMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ConeMesh_H_
#define __ConeMesh_H_

#include "../TrianglesMesh.h"

namespace AKOpenGLEngine {

    class ConeMesh : public TrianglesMesh {

    public:
        ConeMesh(float R = 1.0f, float height = 2.0f, float chop = 1.0f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    };
}


#endif //__ConeMesh_H_
