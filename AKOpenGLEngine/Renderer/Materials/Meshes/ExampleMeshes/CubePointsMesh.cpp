//
//  CubePointsMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/glm.hpp>

#include "CubePointsMesh.h"
#include "../Point.h"

using namespace glm;

namespace AKOpenGLEngine {

    CubePointsMesh::CubePointsMesh(int pointsNumber) : PointsMesh() {
        name = "CubePointsMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);

        unsigned int index = 0;
        for (int i = 0; i < pointsNumber; ++i) {
            for (int j = 0; j < pointsNumber; ++j) {
                for (int k = 0; k < pointsNumber; ++k) {
                    if(pointsNumber == 1) {
                        v.position = vec3(0,0,0);
                    } else {
                        v.position = vec3(((float)i/(pointsNumber-1))*2-1, ((float)j/(pointsNumber-1))*2-1, ((float)k/(pointsNumber-1))*2-1);
                        v.position = vec3(v.position.x,v.position.y,v.position.z);
                    }

                    Point p;
                    p.indices.a = index++;
                    vertices.push_back(v);
                    points.push_back(p);
                }
            }
        }

    }
}
