//
//  PointMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __PointMesh_H_
#define __PointMesh_H_

#include "../PointsMesh.h"

namespace AKOpenGLEngine {

    class PointMesh : public PointsMesh {

    public:
        PointMesh();
    };

}


#endif //__PointMesh_H_
