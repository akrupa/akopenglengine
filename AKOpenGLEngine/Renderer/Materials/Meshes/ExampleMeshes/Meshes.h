//
//  Meshes.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Meshes_H_
#define __Meshes_H_

#include <string>
#include <map>
#include <glm/glm.hpp>

#include "../../../Utilities/Singleton.h"

namespace AKOpenGLEngine {

	class Mesh;

	class Meshes : public Singleton<Meshes> {

		mutable std::map<std::string, Mesh*> meshesMap;

	public:

		Mesh *GetMesh(std::string const& meshName) const;
		Mesh *GetMesh(std::string const& meshName, std::map<std::string, float> const& parameters) const;
		Mesh *GetPointMesh() const;
		Mesh *GetCubePointsMesh(int pointsNumber = 5) const;
		Mesh *GetLineMesh() const;
		Mesh *GetCubeLinesMesh() const;
		Mesh *GetCubeMesh(glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) const;
		Mesh *GetTorusMesh(float R = 1.0f, float r = 0.2f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) const;
		Mesh *GetSphereMesh(unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) const;
		Mesh *GetConeMesh(float R = 1.0f, float height = 2.0f, float chop = 1.0f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) const;
		Mesh *GetTubeMesh(float R = 1.0f, float r = 0.5f, float height = 2.0f, unsigned int density = 40, glm::vec4 const& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) const;
	};
}

#endif //__Meshes_H_
