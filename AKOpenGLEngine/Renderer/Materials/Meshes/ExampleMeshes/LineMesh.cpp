//
//  LineMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/glm.hpp>

#include "LineMesh.h"
#include "../Line.h"

using namespace glm;

namespace AKOpenGLEngine {

    LineMesh::LineMesh() : LinesMesh() {
        name = "LineMesh";
        Vertex v;
        v.normal = vec3(0.0f, 1.0f, 0.0f);
        v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        v.position = vec3(-1,0,0);
        Line l;
        l.indices.a = 0;
        l.indices.a = 1;
        vertices.push_back(v);
        v.position = vec3(1,0,0);
        vertices.push_back(v);
        lines.push_back(l);
    }
}
