//
//  Triangle.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Triangle_H_
#define __Triangle_H_

namespace AKOpenGLEngine {

    struct Triangle {
        union indices {
            struct
            {
                unsigned int a,b,c;
            };
            unsigned int v[3];
        } indices;
    };

}


#endif //__Triangle_H_
