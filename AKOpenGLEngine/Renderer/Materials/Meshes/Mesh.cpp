//
//  MeshRenderer.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Mesh.h"

using namespace std;

namespace AKOpenGLEngine {

    Mesh::Mesh() {
        vertices = vector<Vertex>();
        meshChangeCounter++;
    }

    void Mesh::setVertices(vector<Vertex> vertices) {
        this->vertices = vertices;
        meshChangeCounter++;
    }

    char Mesh::GetMeshChangeHash() {
        return meshChangeCounter;
    }
}
