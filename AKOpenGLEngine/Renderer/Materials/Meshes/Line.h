//
//  Line.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Line_H_
#define __Line_H_

namespace AKOpenGLEngine {

    struct Line {
        union indices {
            struct
            {
                unsigned int a,b;
            };
            unsigned int v[2];
        } indices;
    };

}


#endif //__Line_H_
