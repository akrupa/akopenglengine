//
//  PointsMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __PointsMesh_H_
#define __PointsMesh_H_

#include "Mesh.h"

namespace AKOpenGLEngine {

    struct Point;

    class PointsMesh : public Mesh {

    protected:
        std::vector<Point> points;
        PointsMesh();

    public:
        PointsMesh(std::vector<Vertex> points);
        virtual unsigned int *GetIndexData() override;
        virtual unsigned int GetIndexDataSize() override;

        virtual Vertex *GetVertexData() override;
        virtual unsigned int GetVertexDataSize() override;

        virtual unsigned int GetIndexDrawSize() override;

        virtual GLenum GetDrawType() override;
    };

}


#endif //__PointsMesh_H_
