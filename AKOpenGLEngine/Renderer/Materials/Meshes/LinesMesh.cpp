//
//  LinesMesh.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "LinesMesh.h"
#include "Line.h"

using namespace std;

namespace AKOpenGLEngine {

    LinesMesh::LinesMesh() : Mesh() {
        lines = vector<Line>();
    }

    LinesMesh::LinesMesh(vector<Vertex> points, bool stripLine) {
        this->lines = vector<Line>(stripLine ? points.size()-1 : points.size()/2);
        Line l;

        if(stripLine) {
            for(size_t i=0; i < points.size()-1; ++i) {
                l.indices.a = (unsigned int)i;
                l.indices.b =  (unsigned int)i+1;
                this->lines.push_back(l);
            }
        } else {
            for(size_t i=0; i < points.size()/2; ++i) {
                l.indices.a = (unsigned int)i*2;
                l.indices.b =  (unsigned int)i*2+1;
                this->lines.push_back(l);
            }
        }
        this->vertices = points;
    }

    unsigned int* LinesMesh::GetIndexData() {
        return (unsigned int*)lines.data();
    }

    unsigned int LinesMesh::GetIndexDataSize() {
        return (unsigned int)(lines.size() * sizeof(Line));
    }

    unsigned int LinesMesh::GetIndexDrawSize() {
        return (unsigned int)lines.size()*2;
    }

    Vertex* LinesMesh::GetVertexData() {
        return vertices.data();
    }

    unsigned int LinesMesh::GetVertexDataSize() {
        return (unsigned int)(vertices.size() * sizeof(Vertex));
    }

    GLenum LinesMesh::GetDrawType() {
        return GL_LINES;
    }
}
