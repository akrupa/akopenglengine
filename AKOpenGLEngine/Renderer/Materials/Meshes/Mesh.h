//
//  MeshRenderer.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Mesh_H_
#define __Mesh_H_

#include <vector>
#include <GL/glew.h>
#include <yaml-cpp/yaml.h>

#include "Vertex.h"

namespace AKOpenGLEngine {

    class Mesh {

    protected:
        std::vector<Vertex> vertices;
        char meshChangeCounter = 0;
        std::string name;
        Mesh();

    public:
        virtual unsigned int *GetIndexData() = 0;

        virtual unsigned int GetIndexDataSize() = 0;

        virtual Vertex *GetVertexData() = 0;

        virtual unsigned int GetVertexDataSize() = 0;

        virtual unsigned int GetIndexDrawSize() = 0;

        virtual GLenum GetDrawType() = 0;

        void setVertices(std::vector<Vertex> vertices);

        char GetMeshChangeHash();

        friend YAML::Emitter& operator << (YAML::Emitter& out, const Mesh* c) {
            out << YAML::BeginMap;
            out << YAML::Key << "id" << YAML::Value << (long long int)c;
            out << YAML::Key << "name" << YAML::Value << c->name;
            out << YAML::EndMap;
            return out;
        }
    };

}


#endif //__Mesh_H_
