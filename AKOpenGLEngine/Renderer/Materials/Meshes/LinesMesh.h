//
//  LinesMesh.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __LinesMesh_H_
#define __LinesMesh_H_

#include "Mesh.h"

namespace AKOpenGLEngine {

    struct Line;

    class LinesMesh : public Mesh {

    protected:
        std::vector<Line>lines;
        LinesMesh();
        
    public:
        LinesMesh(std::vector<Vertex>points, bool stripLine = true);
        virtual unsigned int *GetIndexData() override;
        virtual unsigned int GetIndexDataSize() override;

        virtual Vertex *GetVertexData() override;
        virtual unsigned int GetVertexDataSize() override;

        virtual unsigned int GetIndexDrawSize() override;

        virtual GLenum GetDrawType() override;
    };
}


#endif //__LinesMesh_H_
