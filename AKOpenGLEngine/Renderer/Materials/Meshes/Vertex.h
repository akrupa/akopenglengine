//
//  Vertex.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __Vertex_H_
#define __Vertex_H_

#include <glm/glm.hpp>

namespace AKOpenGLEngine {

    struct Vertex {
        glm::vec3 position;
        glm::vec3 tangent;
        glm::vec2 UV;
        glm::vec3 normal;
        glm::vec4 color;
    };

    struct VertexParticleSystem {
        glm::vec3 position;
    };

}

#endif