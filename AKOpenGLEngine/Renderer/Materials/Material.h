//
//  Material.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 19.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Material_H_
#define __Material_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <tuple>
#include <string>

namespace YAML {
    class Node;
    class Emitter;
}

namespace AKOpenGLEngine {

    class Shader;
    class Texture;

    class Material {

        Shader *shader;
        // string - uniform name
        // GLinit - uniform position
        // value
        // bool - object specific uniform
        std::vector<std::tuple<std::string, GLint, glm::vec4, bool>>vectors;
        std::vector<std::tuple<std::string, GLint, float, bool>>floats;
        std::vector<std::tuple<std::string, GLint, int, bool>>integers;
        std::vector<std::tuple<std::string, GLint, glm::mat4, bool>>matrices;
        std::vector<std::tuple<std::string, GLint, Texture *, bool>>textures;

        static Material* lastAppliedMaterial;

    public:
        Material(Shader *shader);

        Material(YAML::Node materialNode);

        void ApplyMaterial();

        void ReleaseMaterial();

        Shader *GetShader() const;

        glm::vec4 GetColor() const;

        glm::vec4 GetColor(std::string const& uniformName) const;

        float GetFloat(std::string const& uniformName) const;

        int GetInt(std::string const& uniformName) const;

        glm::mat4 GetMatrix(std::string const& uniformName) const;

        glm::vec4 GetVector(std::string const& uniformName) const;

        void SetColor(glm::vec4 const& value);

        void SetColor(std::string const& uniformName, glm::vec4 const& value);

        void SetFloat(std::string const& uniformName, float value, bool notifyMissingUniform = true, bool objectSpecific = false);

        void SetInt(std::string const& uniformName, int value, bool notifyMissingUniform = true, bool objectSpecific = false);

        void SetMatrix(std::string const& uniformName, glm::mat4 const& value, bool notifyMissingUniform = true, bool objectSpecific = false);

        void SetVector(std::string const& uniformName, glm::vec4 const& value, bool notifyMissingUniform = true, bool objectSpecific = false);

        void SetTexture(std::string const& uniformName, Texture *value, bool notifyMissingUniform = true, bool objectSpecific = false);

        friend YAML::Emitter& operator << (YAML::Emitter& out, const Material* c);

        bool isUsingLight();
    };
}


#endif //__Material_H_
