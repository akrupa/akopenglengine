//
//  TextureTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TextureTests_H_
#define __TextureTests_H_

#include "../../Tests/RendererTests.h"
#include "../Texture.h"

namespace AKOpenGLEngine {

    class TextureTests : public RendererTests {

    protected:
        Texture *texture;

        virtual void TearDown() override;
    };
}


#endif //__TextureTests_H_
