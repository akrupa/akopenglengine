//
//  TextureTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "TextureTests.h"
#include "../../Managers/TextureManager.h"

namespace AKOpenGLEngine {

    TEST_F(TextureTests, LoadingGoodPNGTexture) {
        texture = TextureManager::getInstance().LoadTexture("sampleTexture.png", GL_BGR, GL_RGB);
        ASSERT_NE(nullptr, texture);
        EXPECT_NE(-1, texture->GetBind());
        EXPECT_EQ("sampleTexture.png", texture->GetName());
        EXPECT_EQ((unsigned int) GL_BGR, (unsigned int) texture->GetImageFormat());
        EXPECT_EQ((unsigned int) GL_RGB, (unsigned int) texture->GetInternalFormat());
    }

    TEST_F(TextureTests, LoadingNotFoundTexture) {
        texture = TextureManager::getInstance().LoadTexture("missing.png", GL_BGR, GL_RGB);
        ASSERT_EQ(nullptr, texture);
    }

    void TextureTests::TearDown() {
        RendererTests::TearDown();
        delete texture;
    }
}
