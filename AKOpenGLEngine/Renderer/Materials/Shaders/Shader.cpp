//
//  Shader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <string>
#include <fstream>
#include <yaml-cpp/yaml.h>

#include "Shader.h"
#include "../../Utilities/GLDebug.h"

using namespace std;

namespace AKOpenGLEngine {

    bool Shader::compileShader(GLuint *shader, GLenum type, string filePath) {

        ifstream fileStream;
        fileStream.open(filePath, ios_base::in);

        string contents((istreambuf_iterator<char>(fileStream)),
                istreambuf_iterator<char>());

        fileStream.close();

        return compileShader(shader, type, contents, filePath);


    }

    bool Shader::compileShader(GLuint *shader, GLenum type, std::string shaderString, string filePath) {
        GLint status;
        const GLchar *source = shaderString.c_str();

        if (!source || strlen(source) <= 0) {
            LOG(ERROR) << "Failed to load shader: " + filePath;
            compiled = false;
            return false;
        }
        GL_CHECK_RETURN(GLuint shaderInt = glCreateShader(type));
        *shader = shaderInt;
        GL_CHECK(glShaderSource(*shader, 1, &source, NULL));
        GL_CHECK(glCompileShader(*shader));

#if defined(_DEBUG)
        GLint logLength;
        GL_CHECK(glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength));
        if (logLength > 1) {
            GLchar *log = (GLchar *) malloc(logLength);
            GL_CHECK(glGetShaderInfoLog(*shader, logLength, &logLength, log));
            LOG(ERROR) << "Shader compile log: " << endl << log;
            free(log);
        }
#endif

        GL_CHECK(glGetShaderiv(*shader, GL_COMPILE_STATUS, &status));
        if (status == 0) {
            GL_CHECK(glDeleteShader(*shader));
            compiled = false;
            return false;
        }
        return true;
    }


    bool Shader::linkProgram(GLuint prog) {
        GLint status;
        GL_CHECK(glLinkProgram(prog));

#if defined(_DEBUG) && !defined(_TEST)
        GLint logLength;
        GL_CHECK(glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength));
        if (logLength > 1) {
            GLchar *log = (GLchar *) malloc(logLength);
            GL_CHECK(glGetProgramInfoLog(prog, logLength, &logLength, log));
            LOG(ERROR) << "Program " << getShaderName() << " link log: " << endl << log;
            free(log);
        }
#endif

        GL_CHECK(glGetProgramiv(prog, GL_LINK_STATUS, &status));
        return status != 0;
    }

    Shader::~Shader() {
        if (m_program) {
            GL_CHECK(glUseProgram(0));
            GL_CHECK(glDeleteProgram(m_program));
            m_program = 0;
        }
    }

    GLuint Shader::getProgram() {
        return m_program;
    }

    GLint Shader::getUniformLocation(string uniform, bool notifyMissingUniform ) {
        auto it = uniformsLocations.find(uniform);
        if (it != uniformsLocations.end()) {
            return it->second;
        }
#if !defined(_TEST)
        if (notifyMissingUniform) {
            LOG(WARNING) << "Couldn't find uniform location: " + uniform;
        }
#endif
        return -1;
    }

    YAML::Emitter &operator<<(YAML::Emitter &out, const Shader *s) {
        out << YAML::BeginMap;
        out << YAML::Key << "id" << YAML::Value << (long long int) s;
        out << YAML::Key << "name" << YAML::Value << s->getShaderName();
        out << YAML::EndMap;
        return out;
    }

    void Shader::fetchUniformLocations() {
        for (auto &uniform : uniforms) {
            GL_CHECK_RETURN(GLint pos = glGetUniformLocation(m_program, uniform.c_str()));
            if (pos < 0) {
                LOG(WARNING) << "Couldn't fetch uniform location: " + uniform;
            } else {
                uniformsLocations.insert(pair<string, GLint>(uniform, pos));
            }
        }
        for (auto &uniform : uniformsBlocks) {
            GL_CHECK_RETURN(GLint pos = glGetUniformBlockIndex(m_program, uniform.c_str()));
            if (pos < 0) {
                LOG(WARNING) << "Couldn't fetch uniform location: " + uniform;
            } else {
                uniformsLocations.insert(pair<string, GLint>(uniform, pos));
            }
        }
    }

    vector<string> Shader::getUniforms() const {
        return uniforms;
    }

    void Shader::attachShaders(GLuint shader1, GLuint shader2) {

        GLuint vertShader = shader1;
        GLuint fragShader = shader2;

        GL_CHECK(glAttachShader(m_program, vertShader));
        GL_CHECK(glAttachShader(m_program, fragShader));

        bindAttributeLocations();

        if (!linkProgram(m_program)) {
            LOG(ERROR) << "Failed to link program: " + getShaderName();

            if (vertShader) {
                GL_CHECK(glDeleteShader(vertShader));
                vertShader = 0;
            }
            if (fragShader) {
                GL_CHECK(glDeleteShader(fragShader));
                fragShader = 0;
            }
            if (m_program) {
                GL_CHECK(glDeleteProgram(m_program));
                m_program = 0;
            }
        }

        fetchUniformLocations();

        if (vertShader) {
            GL_CHECK(glDetachShader(m_program, vertShader));
            GL_CHECK(glDeleteShader(vertShader));
        }

        if (fragShader) {
            GL_CHECK(glDetachShader(m_program, fragShader));
            GL_CHECK(glDeleteShader(fragShader));
        }
    }

    bool Shader::isCompiled() {
        return compiled;
    }
}
