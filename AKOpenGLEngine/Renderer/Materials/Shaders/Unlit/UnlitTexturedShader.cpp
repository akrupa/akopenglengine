//
//  UnlitTexturedShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 01.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "UnlitTexturedShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    string UnlitTexturedShader::getShaderName() const {
        return "UnlitTexturedShader";
    }

    void UnlitTexturedShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void UnlitTexturedShader::fetchUniformLocations() {
        uniforms.push_back("MVPMatrix");
        uniforms.push_back("texture0");
        SurfaceShader::fetchUniformLocations();
    }

    bool UnlitTexturedShader::isUsingLight() {
        return false;
    }
}
