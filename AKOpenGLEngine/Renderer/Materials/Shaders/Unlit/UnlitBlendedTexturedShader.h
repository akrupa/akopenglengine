//
//  UnlitBlendedTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __UnlitBlendedTexturedShader_H_
#define __UnlitBlendedTexturedShader_H_


#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class UnlitBlendedTexturedShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__UnlitBlendedTexturedShader_H_
