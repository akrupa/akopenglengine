//
//  UnlitShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 28.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "UnlitShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {
    string UnlitShader::getShaderName() const {
        return "UnlitShader";
    }

    void UnlitShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void UnlitShader::fetchUniformLocations() {
        uniforms.push_back("MVPMatrix");
        SurfaceShader::fetchUniformLocations();
    }

    bool UnlitShader::isUsingLight() {
        return false;
    }
}