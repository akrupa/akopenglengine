//
//  GeometryShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 04.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <string>

#include "GeometryShader.h"
#include "../../Utilities/GLDebug.h"
#include "../../Utilities/WorkingDirectory.h"

using namespace std;

namespace AKOpenGLEngine {

    void GeometryShader::setup() {
        GLuint vertShader, fragShader, geoShader;
        string vertShaderPathname, fragShaderPathname, geoShaderPathname;


        GL_CHECK_RETURN(m_program = glCreateProgram());

        vertShaderPathname = WorkingDirectory::GetExecutableDirectory()
                + "/shaders/" + getShaderName() + ".vsh";
        if (!compileShader(&vertShader, GL_VERTEX_SHADER, vertShaderPathname)) {
            cout << "Failed to compile vertex shader: " + getShaderName() << endl;
            return;
        }

        fragShaderPathname = WorkingDirectory::GetExecutableDirectory()
                + "/shaders/" + getShaderName() + ".fsh";
        if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fragShaderPathname)) {
            cout << "Failed to compile fragment shader: " + getShaderName() << endl;
            return;
        }

        geoShaderPathname = WorkingDirectory::GetExecutableDirectory()
                + "/shaders/" + getShaderName() + ".gsh";
        if (!compileShader(&geoShader, GL_GEOMETRY_SHADER, geoShaderPathname)) {
            cout << "Failed to compile geometry shader: " + getShaderName() << endl;
            return;
        }

        GL_CHECK(glAttachShader(m_program, vertShader));
        GL_CHECK(glAttachShader(m_program, fragShader));
        GL_CHECK(glAttachShader(m_program, geoShader));

        bindAttributeLocations();

        if (!linkProgram(m_program)) {
            cout << "Failed to link program: " + getShaderName();

            if (vertShader) {
                GL_CHECK(glDeleteShader(vertShader));
                vertShader = 0;
            }
            if (fragShader) {
                GL_CHECK(glDeleteShader(fragShader));
                fragShader = 0;
            }
            if (fragShader) {
                GL_CHECK(glDeleteShader(geoShader));
                fragShader = 0;
            }
            if (m_program) {
                GL_CHECK(glDeleteProgram(m_program));
                m_program = 0;
            }
        }

        fetchUniformLocations();

        if (vertShader) {
            GL_CHECK(glDetachShader(m_program, vertShader));
            GL_CHECK(glDeleteShader(vertShader));
        }

        if (fragShader) {
            GL_CHECK(glDetachShader(m_program, fragShader));
            GL_CHECK(glDeleteShader(fragShader));
        }

        if (geoShader) {
            GL_CHECK(glDetachShader(m_program, geoShader));
            GL_CHECK(glDeleteShader(geoShader));
        }
    }
}