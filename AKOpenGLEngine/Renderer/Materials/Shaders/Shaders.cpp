//
//  Shaders.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 05.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>

#include "Shaders.h"
#include "Shader.h"
#include "Ambient/AmbientShader.h"
#include "Ambient/AmbientTexturedShader.h"
#include "Unlit/UnlitShader.h"
#include "Unlit/UnlitTexturedShader.h"
#include "Unlit/UnlitBlendedTexturedShader.h"
#include "Miscellaneous/DebugNormalsShader.h"
#include "Diffuse/DiffuseShader.h"
#include "Diffuse/DiffuseTexturedShader.h"
#include "DiffuseSpecular/DiffuseSpecularShader.h"
#include "DiffuseSpecular/DiffuseSpecularTexturedShader.h"
#include "Normal/NormalDiffuseShader.h"
#include "Normal/NormalDiffuseSpecularShader.h"
#include "Particles/UnlitParticlesShader.h"
#include "Particles/UnlitParticlesTexturedShader.h"

using namespace std;

namespace AKOpenGLEngine {

    void Shaders::ReleaseShaders() {
        for( auto& shader : shadersMap) {
            delete shader.second;
        }
        shadersMap.clear();
    }

    Shader *Shaders::GetShader(string shaderName) {
        auto it = shadersMap.find(shaderName);
        if(it != shadersMap.end()) {
            return it->second;
        } else {
            Shader *shader = nullptr;

            if(shaderName == "AmbientShader") {
                shader = new AmbientShader();
                shader ->setup();
            } else if(shaderName == "AmbientTexturedShader") {
                shader = new AmbientTexturedShader();
                shader ->setup();
            } else if(shaderName == "UnlitShader") {
                shader = new UnlitShader();
                shader ->setup();
            } else if(shaderName == "UnlitTexturedShader") {
                shader = new UnlitTexturedShader();
                shader ->setup();
            } else if(shaderName == "UnlitBlendedTexturedShader") {
                shader = new UnlitBlendedTexturedShader();
                shader ->setup();
            } else if(shaderName == "DebugNormalsShader") {
                shader = new DebugNormalsShader();
                shader ->setup();
            } else if(shaderName == "DiffuseShader") {
                shader = new DiffuseShader();
                shader ->setup();
            } else if(shaderName == "DiffuseSpecularShader") {
                shader = new DiffuseSpecularShader();
                shader ->setup();
            }  else if(shaderName == "DiffuseSpecularTexturedShader") {
                shader = new DiffuseSpecularTexturedShader();
                shader ->setup();
            } else if(shaderName == "DiffuseTexturedShader") {
                shader = new DiffuseTexturedShader();
                shader ->setup();
            } else if(shaderName == "NormalDiffuseShader") {
                shader = new NormalDiffuseShader();
                shader ->setup();
            } else if(shaderName == "NormalDiffuseSpecularShader") {
                shader = new NormalDiffuseSpecularShader();
                shader ->setup();
            } else if(shaderName == "UnlitParticlesShader") {
                shader = new UnlitParticlesShader();
                shader ->setup();
            } else if(shaderName == "UnlitParticlesTexturedShader") {
                shader = new UnlitParticlesTexturedShader();
                shader ->setup();
            }

            if(shader != nullptr) {
                shadersMap.insert(std::pair<string, Shader *>(shaderName, shader));
            } else {
#if !defined(_TEST)
                LOG(ERROR) << "Couldn't find shader " << shaderName;
#endif
            }
            return shader;
        }
    }

    Shader *Shaders::GetAmbientShader() {
        return GetShader("AmbientShader");
    }

    Shader *Shaders::GetAmbientTexturedShader() {
        return GetShader("AmbientTexturedShader");
    }

    Shader *Shaders::GetDebugNormalsShader() {
        return GetShader("DebugNormalsShader");
    }

    Shader *Shaders::GetUnlitBlendedTexturedShader() {
        return GetShader("UnlitBlendedTexturedShader");
    }

    Shader *Shaders::GetUnlitShader() {
        return GetShader("UnlitShader");
    }

    Shader *Shaders::GetUnlitTexturedShader() {
        return GetShader("UnlitTexturedShader");
    }

    Shader *Shaders::GetDiffuseShader() {
        return GetShader("DiffuseShader");
    }

    Shader *Shaders::GetDiffuseSpecularShader() {
        return GetShader("DiffuseSpecularShader");
    }

    Shader *Shaders::GetDiffuseTexturedShader() {
        return GetShader("DiffuseTexturedShader");
    }

    Shader *Shaders::GetDiffuseSpecularTexturedShader() {
        return GetShader("DiffuseSpecularTexturedShader");
    }

    Shader *Shaders::GetNormalDiffuseShader() {
        return GetShader("NormalDiffuseShader");
    }

    Shader *Shaders::GetNormalDiffuseSpecularShader() {
        return GetShader("NormalDiffuseSpecularShader");
    }

    Shader *Shaders::GetUnlitParticlesShader() {
        return GetShader("UnlitParticlesShader");
    }

    Shader *Shaders::GetUnlitParticlesTexturedShader() {
        return GetShader("UnlitParticlesTexturedShader");
    }
}
