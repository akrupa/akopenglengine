//
//  VertexAttrib.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <GL/glew.h>

namespace AKOpenGLEngine {

    typedef enum : GLuint {
        VertexAttribPosition,
        VertexAttribNormal,
        VertexAttribUV,
        VertexAttribTangent,
        VertexAttribColor
    } VertexAttrib;

}