//
//  AmbientTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 18.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __AmbientTexturedShader_H_
#define __AmbientTexturedShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class AmbientTexturedShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__AmbientTexturedShader_H_
