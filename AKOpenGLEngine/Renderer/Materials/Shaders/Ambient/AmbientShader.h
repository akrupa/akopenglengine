//
//  AmbientShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __AmbientShader_H_
#define __AmbientShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class AmbientShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };

}


#endif //__AmbientShader_H_
