//
//  ShaderTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ShaderTests_H_
#define __ShaderTests_H_

#include "../../../Tests/RendererTests.h"
#include "../Shader.h"

namespace AKOpenGLEngine {

    class ShaderTests : public RendererTests {

    protected:
        Shader *shader;

        virtual void TearDown() override;
    };
}


#endif //__ShaderTests_H_
