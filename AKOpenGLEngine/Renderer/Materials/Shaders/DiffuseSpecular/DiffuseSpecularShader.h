//
//  DiffuseSpecularShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __DiffuseSpecularShader_H_
#define __DiffuseSpecularShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class DiffuseSpecularShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__DiffuseSpecularShader_H_
