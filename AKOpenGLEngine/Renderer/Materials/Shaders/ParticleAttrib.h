//
//  VertexAttrib.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 02.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <GL/glew.h>

namespace AKOpenGLEngine {

    typedef enum : GLuint {
        ParticleAttribPosition,
        ParticleAttribVertex
    } ParticleAttrib;

}