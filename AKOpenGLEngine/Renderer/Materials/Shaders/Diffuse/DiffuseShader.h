//
//  DiffuseShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 14.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __DiffuseShader_H_
#define __DiffuseShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class DiffuseShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__DiffuseShader_H_
