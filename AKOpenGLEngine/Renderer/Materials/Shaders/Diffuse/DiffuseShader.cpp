//
//  DiffuseShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 14.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "DiffuseShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string DiffuseShader::getShaderName() const {
        return "DiffuseShader";
    }

    void DiffuseShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void DiffuseShader::fetchUniformLocations() {
        uniforms.push_back("MVPMatrix");
        uniforms.push_back("NormalMatrix");
        uniforms.push_back("ModelViewMatrix");
        uniforms.push_back("Color");
        uniformsBlocks.push_back("Lights");
        SurfaceShader::fetchUniformLocations();
    }

    bool DiffuseShader::isUsingLight() {
        return true;
    }
}