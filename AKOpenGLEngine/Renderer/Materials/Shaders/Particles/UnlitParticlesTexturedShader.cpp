//
//  UnlitParticlesTexturedShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "UnlitParticlesTexturedShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../ParticleAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string UnlitParticlesTexturedShader::getShaderName() const {
        return "UnlitParticlesTexturedShader";
    }

    void UnlitParticlesTexturedShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, ParticleAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, ParticleAttribVertex, "in_Vertex"));
    }

    void UnlitParticlesTexturedShader::fetchUniformLocations() {
        uniforms.push_back("VPMatrix");
        uniforms.push_back("CameraRightWorldSpace");
        uniforms.push_back("CameraUpWorldspace");
        uniforms.push_back("texture0");
        SurfaceShader::fetchUniformLocations();
    }

    bool UnlitParticlesTexturedShader::isUsingLight() {
        return false;
    }
}