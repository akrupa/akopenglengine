//
//  UnlitParticlesTexturedShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __UnlitParticlesTexturedShader_H_
#define __UnlitParticlesTexturedShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class UnlitParticlesTexturedShader : public SurfaceShader  {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__UnlitParticlesTexturedShader_H_
