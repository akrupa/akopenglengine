//
//  UnlitParticlesShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 20.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __UnlitParticlesShader_H_
#define __UnlitParticlesShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class UnlitParticlesShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__UnlitParticlesShader_H_
