#version 410 core

#define M_PI 3.1415926535897932384626433832795

in vec4 pass_Color;
in vec2 pass_UV;

layout(location = 0) out vec4 out_Color;

uniform sampler2D texture0;

void main(void) {

    float A=1;
    float a=20;
    float c=20;
    float b=0;
    float xy = A * exp(-(a*(pass_UV.x-0.5)*(pass_UV.x-0.5)+2*b*(pass_UV.x-0.5)*(pass_UV.y-0.5) + c*(pass_UV.y-0.5)*(pass_UV.y-0.5))+1);
    vec4 col = vec4(xy, xy, xy,1);
	out_Color = pass_Color*col;
	//out_Color = pass_Color * texture( texture0, pass_UV );
}