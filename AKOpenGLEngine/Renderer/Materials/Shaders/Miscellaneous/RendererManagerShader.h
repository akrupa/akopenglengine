//
//  RendererManagerShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 06.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __RendererManagerShader_H_
#define __RendererManagerShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class RendererManagerShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };

}


#endif //__RendererManagerShader_H_
