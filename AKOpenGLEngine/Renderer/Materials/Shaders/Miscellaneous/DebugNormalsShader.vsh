#version 410
in vec4 in_Position;
in vec4 in_Normal;

out Vertex
{
  vec4 normal;
  vec4 color;
} vertex;

void main()
{
  gl_Position = in_Position;
  vertex.normal = in_Normal;
  vertex.color =  vec4(0.0, 0.0, 1.0, 1.0);
}