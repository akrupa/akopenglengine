//
//  NormalDiffuseShader.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __NormalDiffuseShader_H_
#define __NormalDiffuseShader_H_

#include "../SurfaceShader.h"

namespace AKOpenGLEngine {

    class NormalDiffuseShader : public SurfaceShader {

    protected:
        virtual std::string getShaderName() const override final;
        virtual void bindAttributeLocations() override;
        virtual void fetchUniformLocations() override;
        virtual bool isUsingLight() override;
    };
}


#endif //__NormalDiffuseShader_H_
