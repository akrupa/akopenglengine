#version 410 core

in vec4 pass_Color;
in vec2 pass_UV;
in vec3 pass_Position;
in vec3 passLightDirections[4];
in vec3 passLightPosition[4];

layout(location = 0) out vec4 out_Color;

/*
enum LightType : int {
    Directional,
    Point,
    Spot
};
*/

uniform sampler2D texture0;
uniform sampler2D textureNormal;

struct Light {
    vec4 typeRangeSpotAngleIntensity;
    vec4 direction;
    vec4 position;
    vec4 color;
};

layout (shared) uniform Lights {
    int numberOfLights;
    Light light[4];
};

void computeLight(int lightNumber, vec3 normal, vec3 v, out float diffuseFactor) {
    vec3 s = vec3(0);
    diffuseFactor = 0;
    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 0) { //DirectionalLight
        s = passLightDirections[lightNumber];
    } else { //Point or spot light
        s = normalize(light[lightNumber].position.xyz-pass_Position);
    }

    if(light[lightNumber].typeRangeSpotAngleIntensity.x == 2) { //SpotLight
        float angle = acos(dot(s, passLightDirections[lightNumber]));
        if (angle < light[lightNumber].typeRangeSpotAngleIntensity.z) {
            diffuseFactor = dot(normal, s);
        }
    } else {
        diffuseFactor = dot(normal, s);
    }
}

void main() {
	vec4 DiffuseColor = vec4(0);
	vec4 normal = 2.0 * texture(textureNormal, pass_UV) - 1.0;
    vec3 v = normalize(-pass_Position);

	for(int i = 0; i < numberOfLights; i++) {
	    float diffuseFactor = 0;
	    computeLight(i, normal.xyz, v, diffuseFactor);
	    float lightIntensity = light[i].typeRangeSpotAngleIntensity.w;
        if (diffuseFactor > 0) {
            DiffuseColor += light[i].color * lightIntensity * diffuseFactor;
        }
    }
	out_Color = min(pass_Color*DiffuseColor, 1.0) * texture( texture0, pass_UV );
	//out_Color = texture( texture0, pass_UV );
}