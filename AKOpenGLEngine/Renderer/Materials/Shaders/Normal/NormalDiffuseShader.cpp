//
//  NormalDiffuseShader.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "NormalDiffuseShader.h"
#include "../../../Utilities/GLDebug.h"
#include "../VertexAttrib.h"

using namespace std;

namespace AKOpenGLEngine {

    string NormalDiffuseShader::getShaderName() const {
        return "NormalDiffuseShader";
    }

    void NormalDiffuseShader::bindAttributeLocations() {
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribPosition, "in_Position"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribUV, "in_UV"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribTangent, "in_Tangent"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal"));
        GL_CHECK(glBindAttribLocation(m_program, VertexAttribColor, "in_Color"));
    }

    void NormalDiffuseShader::fetchUniformLocations() {
        uniforms.push_back("MVPMatrix");
        uniforms.push_back("NormalMatrix");
        uniforms.push_back("Color");
        uniforms.push_back("ModelViewMatrix");
        uniformsBlocks.push_back("Lights");
        uniforms.push_back("texture0");
        uniforms.push_back("textureNormal");
        SurfaceShader::fetchUniformLocations();
    }

    bool NormalDiffuseShader::isUsingLight() {
        return true;
    }
}