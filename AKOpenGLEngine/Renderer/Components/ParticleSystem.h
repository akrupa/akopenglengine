//
//  ParticleSystem.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 20.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ParticleSystem_H_
#define __ParticleSystem_H_

#include <vector>
#include <GL/glew.h>

#include "Component.h"
#include "Particle.h"
#include "../Materials/Meshes/Vertex.h"

namespace AKOpenGLEngine {

    class Material;

    class ParticleSystem : public Component {

        static constexpr GLfloat billboardData[] = {
                -0.5f, -0.5f, 0.0f,
                0.5f, -0.5f, 0.0f,
                -0.5f, 0.5f, 0.0f,
                0.5f, 0.5f, 0.0f,
        };

        const unsigned int particlesHistory = 1;

        GLuint m_VAO;

        GLuint billboardBuffer;
        GLuint particlesBuffer;

        float duration = 5.0f;
        bool looping = true;
        unsigned int maxParticles = 3000;
        float maxParticleLifeTime = 50000000;
        float particlesEmmisionRate = 100000000;

        float particlesToSpawnCounter = 0;


        std::vector<Particle> particles[1];
        std::vector<VertexParticleSystem> gpuParticles;
        Material *material;

        GLuint allocatedVertexBuffer = 0;

        void SpawnParticle();
        void GenerateBuffers();
        void FillData();
        void UpdateParticles(float dt);

        Particle UpdateParticleEulerODE(Particle p, float dt);

        std::vector<Particle>& getParticles(unsigned int history);
        unsigned int currentVectorParticlesCounter = 0;

    public:
        virtual std::string GetName() const override;
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight) override;
        virtual void printYaml(YAML::Emitter& out) const override;
        virtual void FixedUpdate(float dt) override;
        ParticleSystem(Material *material);
        std::vector<Particle>& GetParticles();
    };
}


#endif //__ParticleSystem_H_
