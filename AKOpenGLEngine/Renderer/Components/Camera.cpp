//
//  Camera.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <yaml-cpp/yaml.h>

#include "Camera.h"
#include "../GameObject.h"
#include "Transform.h"
#include "../Utilities/GLDebug.h"
#include "../Managers/ReferenceManager.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

#pragma mark Constructors

    Camera::Camera() {
        clearFlag = CameraClearFlag::SolidColor;
        clearColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);
        aspectRatio = 4.0f / 3.0f;
        projectionMatrix = perspective((float) M_PI_4, aspectRatio, 0.1f, 1000.0f);
        cachedTranslationMatrix = mat4(0);
    }

    Camera::Camera(Node &initNode) {
        new(this) Camera();
        clearFlag = (CameraClearFlag) initNode["clearFlag"].as<int>();
        aspectRatio = 4.0f / 3.0f;

        Node clearColorNode = initNode["clearColor"];
        vec4 v;
        for (int i = 0; i < 4; i++) {
            v[i] = clearColorNode[i].as<float>();
        }
        clearColor = v;
    }

#pragma mark Virtual methods

    string Camera::GetName() const {
        return "Camera";
    }

    void Camera::printYaml(Emitter &out) const {
        Component::printYaml(out);
        out << Key << "clearFlag" << Value << (int) clearFlag;
        out << Key << "clearColor" << Value << clearColor;
    }

#pragma mark Getters

    CameraClearFlag Camera::getCameraClearFlag() const {
        return clearFlag;
    }

    glm::vec4 Camera::getCameraClearColor() const {
        return clearColor;
    }

    mat4 Camera::getVPMatrix() const {
        mat4 translationMatrix = gameObject->GetTransform()->getViewMatrix();
        if (cachedTranslationMatrix != translationMatrix) {
            cachedTranslationMatrix = translationMatrix;
            cachedVPMatrix = projectionMatrix * translationMatrix;
        }
        return cachedVPMatrix;
    }

    mat4 Camera::getProjectionMatrix() const {
        return projectionMatrix;
    }

    mat4 Camera::getViewMatrix() const {
        return gameObject->GetTransform()->getViewMatrix();
    }

    mat4 Camera::getModelMatrix() const {
        return gameObject->GetTransform()->getModelMatrix();
    }

    float Camera::getAspect() const {
        return aspectRatio;
    }

#pragma mark Setters

    void Camera::setCameraClearFlag(CameraClearFlag clearFlag) {
        this->clearFlag = clearFlag;
    }

    void Camera::setCameraClearColor(vec4 const& clearColor) {
		vec4 cc = glm::clamp(clearColor, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));
        this->clearColor = cc;
    }

    void Camera::setAspect(float aspect) {
        aspectRatio = aspect;
        projectionMatrix = perspective((float) M_PI_4, aspectRatio, 0.1f, 1000.0f);
        Transform *t = gameObject->GetTransform();
        t->setPosition(t->getPosition());
        mat4 translationMatrix = t->getModelMatrix();
        cachedTranslationMatrix = translationMatrix;
        cachedVPMatrix = projectionMatrix * translationMatrix;
    }

#pragma mark Other methods

    void Camera::Clear() {
        switch (clearFlag) {
            case CameraClearFlag::DontClear:
                break;
            case CameraClearFlag::SolidColor:
                GL_CHECK(glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w));
                GL_CHECK(glDepthMask(GL_TRUE));
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                break;
            case CameraClearFlag::OnlyDepth:
                GL_CHECK(glDepthMask(GL_TRUE));
                GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));
                break;
        }

    }
}
