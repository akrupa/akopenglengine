//
//  Light.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "Light.h"
#include <yaml-cpp/yaml.h>

#include "../Managers/ReferenceManager.h"
#include "Transform.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

#pragma mark Private Methods

    void Light::UpdateLightDirection() const {
        light.direction = vec3(1.0f, 0.0f, 0.0f) * GetTransform()->getRotation();
    }

    void Light::UpdateLightPosition() const {
        light.position = GetTransform()->getPosition();
    }

#pragma mark Constructors

    Light::Light() {
        light.type = LightType::Directional;
        light.direction = vec3(1.0f, 0.0f, 0.0f);
        light.position = vec3(0.0f, 0.0f, 0.0f);
        light.range = 10;
        light.spotAngle = 0;
        light.intensity = 1;
        light.color = vec3(201.0f / 255.0f, 226.0f / 255.0f, 255.0f / 255.0f);
    }

    Light::Light(YAML::Node &initNode) {
        new(this) Light();
        light.type = (LightType) initNode["type"].as<int>();
        light.range = initNode["range"].as<float>();
        light.spotAngle = initNode["spotAngle"].as<float>();
        light.intensity = initNode["intensity"].as<float>();

        Node colorNode = initNode["color"];
        vec3 v;
        for (int i = 0; i < 3; i++) {
            v[i] = colorNode[i].as<float>();
        }
        light.color = v;
    }

#pragma mark Getters

    LightType Light::getLightType() const {
        return light.type;
    }

    vec3 Light::getLightDirection() const {
        UpdateLightDirection();
        return light.direction;
    }

    vec3 Light::getLightPosition() const {
        UpdateLightPosition();
        return light.position;
    }

    float Light::getLightRange() const {
        return light.range;
    }

    float Light::getLightSpotAngle() const {
        return light.spotAngle;
    }

    float Light::getLightIntensity() const {
        return light.intensity;
    }

    vec3 Light::getLightColor() const {
        return light.color;
    }

    LightStruct Light::getLightStruct() const {
        UpdateLightDirection();
        UpdateLightPosition();
        return light;
    }

#pragma mark Setters

    void Light::setLightType(LightType lightType) {
        light.type = lightType;
    }

    void Light::setLightRange(float lightRange) {
        lightRange = glm::max(0.0f, lightRange);
        light.range = lightRange;
    }

    void Light::setLightSpotAngle(float lightSpotAngle) {
        lightSpotAngle = glm::clamp(lightSpotAngle, 0.0f, (float)M_PI_2);
        light.spotAngle = lightSpotAngle;
    }

    void Light::setLightIntensity(float lightIntensity) {
        lightIntensity = glm::max(0.0f, lightIntensity);
        light.intensity = lightIntensity;
    }

    void Light::setLightColor(vec3 lightColor) {
        lightColor = max(vec3(0,0,0), min(vec3(1,1,1), lightColor));
        light.color = lightColor;
    }

#pragma mark Virtual methods

    std::string Light::GetName() const {
        return "Light";
    }

    void Light::printYaml(YAML::Emitter &out) const {
        Component::printYaml(out);
        out << Key << "type" << Value << (int)light.type;
        out << Key << "range" << Value << light.range;
        out << Key << "spotAngle" << Value << light.spotAngle;
        out << Key << "intensity" << Value << light.intensity;
        out << Key << "color" << Value << light.color;
    }
}
