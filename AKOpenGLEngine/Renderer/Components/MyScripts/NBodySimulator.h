//
//  NBodySimulator.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __NBodySimulator_H_
#define __NBodySimulator_H_

#include <vector>

#include "../ScriptBehaviour.h"
#include "../Particle.h"

namespace AKOpenGLEngine {

    class ParticleSystem;

    class NBodySimulator : public ScriptBehaviour {

        ParticleSystem *ps;
        const double G = 6.674 * 10e-11;
        const float softeningFactor = 0.05f;
        const float softeningFactorSquared = softeningFactor * softeningFactor;
    public:
        virtual std::string GetName() const override;

        NBodySimulator();

        virtual void Start() override;

        virtual void FixedUpdate(float dt) override;
        void updateParticlesInThread(std::vector<Particle> &particles, unsigned int from, unsigned int to);

    };
}


#endif //__NBodySimulator_H_
