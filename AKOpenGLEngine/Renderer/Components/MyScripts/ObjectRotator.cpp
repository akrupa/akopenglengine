//
//  ObjectRotator.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <iostream>
#include <yaml-cpp/yaml.h>

#include "ObjectRotator.h"
#include "../Transform.h"
#include "../../Managers/ReferenceManager.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

    ObjectRotator::ObjectRotator() : ScriptBehaviour() {
        rotationSpeed = vec3(0.5,0,0);
    }

    ObjectRotator::ObjectRotator(Node node) {
        new(this)ObjectRotator();
        Node rotationSpeedNode = node["rotationSpeed"];
        vec3 v;
        for (int i = 0; i < 3; i++) {
            v[i] = rotationSpeedNode[i].as<float>();
        }
        rotationSpeed = v;
    }

    string ObjectRotator::GetName() const {
        return "ObjectRotator";
    }

    void ObjectRotator::Update(float dt) {
        ScriptBehaviour::Update(dt);
        Transform *transform = GetTransform();
        transform->setRotation(transform->getRotationInEulerAngles()+rotationSpeed*dt);
    }

    void ObjectRotator::printYaml(Emitter &out) const {
        ScriptBehaviour::printYaml(out);
        out << Key << "rotationSpeed" << Value << rotationSpeed;
    }
}
