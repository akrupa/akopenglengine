//
//  NBodySimulator.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <thread>

#include "NBodySimulator.h"
#include "../ParticleSystem.h"
#include "../../GameObject.h"

using namespace glm;
using namespace std;

namespace AKOpenGLEngine {

    NBodySimulator::NBodySimulator() {

    }

    void NBodySimulator::Start() {
        ScriptBehaviour::Start();

        ps = gameObject->GetComponent<ParticleSystem>();
    }

    void NBodySimulator::FixedUpdate(float dt) {
        ScriptBehaviour::FixedUpdate(dt);

        unsigned concurentThreadsSupported = thread::hardware_concurrency();
        int threadsNumber = std::max(1, (int)concurentThreadsSupported);
        //threadsNumber = 1;
        vector<thread> threads = vector<thread>();
        vector<Particle> &particles = ps->GetParticles();
        unsigned int perThread = (unsigned int)particles.size()/threadsNumber;
        for (int i = 0; i < threadsNumber; ++i) {
            unsigned int from = i*perThread;
            unsigned int to = (i+1)*perThread;
            if(i==threadsNumber-1) {
                to = (unsigned int)particles.size();
            }
            threads.push_back(thread(&NBodySimulator::updateParticlesInThread, this, ref(particles), ref(from), ref(to)));
        }
        for (auto &thread : threads) {
            thread.join();
        }
    }


    string NBodySimulator::GetName() const {
        return "NBodySimulator";
    }

    void NBodySimulator::updateParticlesInThread(std::vector<Particle> &particles, unsigned int from, unsigned int to) {
        for (unsigned int i = from; i < to; ++i) {
            particles[i].force = vec3(0);
        }

        for (unsigned int i = from; i < to; ++i) {

            for (unsigned long j = 0; j < particles.size(); ++j) {
                vec3 rij = particles[j].position - particles[i].position;
                float distSquared = glm::length2(rij);
                float denominatorSqrt = 1.0f / sqrt(distSquared + softeningFactorSquared);
                float denominator = denominatorSqrt * denominatorSqrt * denominatorSqrt;
                vec3 f = (rij * particles[j].mass * particles[i].mass) * denominator;
                particles[i].force += f;
            }

            particles[i].force *= G;
        }
    }
}
