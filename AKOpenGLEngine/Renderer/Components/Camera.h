//
//  Camera.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Camera_H_
#define __Camera_H_

#include <glm/glm.hpp>

#include "Component.h"

namespace AKOpenGLEngine {

    enum class CameraClearFlag {
        DontClear,
        SolidColor,
        OnlyDepth
    };

    class Camera : public Component {

    private:

#pragma mark Serialized values

        CameraClearFlag clearFlag;
        glm::vec4 clearColor;

#pragma mark Runtime values

        float aspectRatio;
        mutable glm::mat4 cachedTranslationMatrix;
        mutable glm::mat4 cachedVPMatrix;
        glm::mat4 projectionMatrix;

    public:

#pragma mark Constructors

        Camera();
        Camera(YAML::Node& initNode);

#pragma mark Virtual methods

        virtual std::string GetName() const override final;
        virtual void printYaml(YAML::Emitter& out) const override;

#pragma mark Getters

        CameraClearFlag getCameraClearFlag() const;
        glm::vec4 getCameraClearColor() const;
        glm::mat4 getVPMatrix() const;
        glm::mat4 getProjectionMatrix() const;
        glm::mat4 getViewMatrix() const;
        glm::mat4 getModelMatrix() const;
        float getAspect() const;

#pragma mark Setters

        void setCameraClearFlag(CameraClearFlag clearFlag);
        void setCameraClearColor(glm::vec4 const& clearColor);
        void setAspect(float aspect);

#pragma mark Other methods

        void Clear();

    };
}


#endif //__Camera_H_
