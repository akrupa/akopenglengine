//
//  LightTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>

#include "LightTests.h"
#include "../Light.h"

using namespace glm;
using namespace YAML;
using namespace std;

namespace AKOpenGLEngine {

    TEST_F(LightTests, CreatingLight) {
        ASSERT_NE(nullptr, light);
        ASSERT_EQ("Light", light->GetName());
    }

    TEST_F(LightTests, LightType) {
        light->setLightType(LightType::Directional);
        EXPECT_EQ(LightType::Directional, light->getLightType());
        EXPECT_NE(LightType::Point, light->getLightType());

        light->setLightType(LightType::Point);
        EXPECT_EQ(LightType::Point, light->getLightType());

        light->setLightType(LightType::Spot);
        EXPECT_EQ(LightType::Spot, light->getLightType());
    }

    TEST_F(LightTests, LightRange) {
        light->setLightRange(-13.3f);
        EXPECT_FLOAT_EQ(0, light->getLightRange());

        light->setLightRange(13.3);
        EXPECT_FLOAT_EQ(13.3, light->getLightRange());
    }

    TEST_F(LightTests, LightSpotAngle) {
        light->setLightSpotAngle(-13.3f);
        EXPECT_FLOAT_EQ(0, light->getLightSpotAngle());

        light->setLightSpotAngle(M_PI_4);
        EXPECT_FLOAT_EQ(M_PI_4, light->getLightSpotAngle());

        light->setLightSpotAngle(1333.0f);
        EXPECT_FLOAT_EQ(M_PI_2, light->getLightSpotAngle());
    }

    TEST_F(LightTests, LightIntensity) {
        light->setLightIntensity(-13.3f);
        EXPECT_FLOAT_EQ(0, light->getLightIntensity());

        light->setLightIntensity(13.3f);
        EXPECT_FLOAT_EQ(13.3f, light->getLightIntensity());
    }

    TEST_F(LightTests, LightColor) {
        light->setLightColor(vec3(1, 0, 0.4));
        EXPECT_FLOAT_EQ(1, light->getLightColor().x);
        EXPECT_FLOAT_EQ(0, light->getLightColor().y);
        EXPECT_FLOAT_EQ(0.4, light->getLightColor().z);

        light->setLightColor(vec3(2, 0, -0.4));
        EXPECT_FLOAT_EQ(1, light->getLightColor().x);
        EXPECT_FLOAT_EQ(0, light->getLightColor().y);
        EXPECT_FLOAT_EQ(0, light->getLightColor().z);
    }

    TEST_F(LightTests, LightYAMLSerialization) {
        YAML::Emitter out;

        LightType type = LightType::Spot;
        float range = 2;
        float spotAngle = (float)M_PI_4;
        float intensity = 1.33f;
        glm::vec3 color = vec3(0, 0.33, 0.67);

        light->setLightType(type);
        light->setLightRange(range);
        light->setLightSpotAngle(spotAngle);
        light->setLightIntensity(intensity);
        light->setLightColor(color);

        out<<light;
        string serializedLight = out.c_str();
        YAML::Node lightNode = YAML::Load(serializedLight);
        Light *light1 = new Light(lightNode);

        EXPECT_EQ(type, light1->getLightType());
        EXPECT_FLOAT_EQ(range, light1->getLightRange());
        EXPECT_FLOAT_EQ(spotAngle, light1->getLightSpotAngle());
        EXPECT_FLOAT_EQ(intensity, light1->getLightIntensity());

        EXPECT_FLOAT_EQ(color.x, light1->getLightColor().x);
        EXPECT_FLOAT_EQ(color.y, light1->getLightColor().y);
        EXPECT_FLOAT_EQ(color.z, light1->getLightColor().z);

        delete light1;
    }

    void LightTests::SetUp() {
        Test::SetUp();
        light = new Light();
        ASSERT_NE(nullptr, light);
    }

    void LightTests::TearDown() {
        Test::TearDown();
        delete light;
    }

}