//
//  CameraTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>

#include "CameraTests.h"
#include "../Camera.h"

using namespace glm;
using namespace YAML;
using namespace std;

namespace AKOpenGLEngine {

	TEST_F(CameraTests, CreatingCamera) {
		ASSERT_NE(nullptr, camera);
		ASSERT_EQ("Camera", camera->GetName());
	}

	TEST_F(CameraTests, CameraClearFlagType) {
		camera->setCameraClearFlag(CameraClearFlag::DontClear);
		EXPECT_EQ(CameraClearFlag::DontClear, camera->getCameraClearFlag());
		EXPECT_NE(CameraClearFlag::OnlyDepth, camera->getCameraClearFlag());

		camera->setCameraClearFlag(CameraClearFlag::OnlyDepth);
		EXPECT_EQ(CameraClearFlag::OnlyDepth, camera->getCameraClearFlag());

		camera->setCameraClearFlag(CameraClearFlag::SolidColor);
		EXPECT_EQ(CameraClearFlag::SolidColor, camera->getCameraClearFlag());
	}

	TEST_F(CameraTests, CameraClearColor) {
        camera->setCameraClearColor(vec4(1, 0, 0.4, 1));
        EXPECT_FLOAT_EQ(1, camera->getCameraClearColor().x);
        EXPECT_FLOAT_EQ(0, camera->getCameraClearColor().y);
        EXPECT_FLOAT_EQ(0.4, camera->getCameraClearColor().z);
        EXPECT_FLOAT_EQ(1, camera->getCameraClearColor().w);

        camera->setCameraClearColor(vec4(2, 0, -0.4, 1));
        EXPECT_FLOAT_EQ(1, camera->getCameraClearColor().x);
        EXPECT_FLOAT_EQ(0, camera->getCameraClearColor().y);
        EXPECT_FLOAT_EQ(0, camera->getCameraClearColor().z);
        EXPECT_FLOAT_EQ(1, camera->getCameraClearColor().w);
	}

    TEST_F(CameraTests, CameraYAMLSerialization) {
        Emitter out;
        CameraClearFlag clearFlag = CameraClearFlag::SolidColor;
        vec4 clearColor = vec4(0, 0.33, 0.67, 1);

        camera->setCameraClearFlag(clearFlag);
        camera->setCameraClearColor(clearColor);

        out<<camera;
        string serializedCamera = out.c_str();
        Node cameraNode = Load(serializedCamera);
        Camera *camera = new Camera(cameraNode);

        EXPECT_EQ(clearFlag, camera->getCameraClearFlag());
        EXPECT_FLOAT_EQ(clearColor.x, camera->getCameraClearColor().x);
        EXPECT_FLOAT_EQ(clearColor.y, camera->getCameraClearColor().y);
        EXPECT_FLOAT_EQ(clearColor.z, camera->getCameraClearColor().z);
        EXPECT_FLOAT_EQ(clearColor.w, camera->getCameraClearColor().w);
    }

	void CameraTests::SetUp() {
		Test::SetUp();
		camera = new Camera();
		ASSERT_NE(nullptr, camera);
	}

	void CameraTests::TearDown() {
		Test::TearDown();
		delete camera;
	}
}
