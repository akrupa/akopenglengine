//
//  TransformTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TransformTests_H_
#define __TransformTests_H_

#include "../../../gtest/gtest.h"

namespace AKOpenGLEngine {

    class Transform;

    class TransformTests : public ::testing::Test {

    protected:
        Transform* transform;
        virtual void SetUp();
        virtual void TearDown();
    };
}


#endif //__TransformTests_H_
