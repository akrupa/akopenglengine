//
//  LightTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __LightTests_H_
#define __LightTests_H_

#include "../../../gtest/gtest.h"

namespace AKOpenGLEngine {

    class Light;

    class LightTests : public ::testing::Test {

    protected:
        Light* light;
        virtual void SetUp();
        virtual void TearDown();
    };
}


#endif //__LightTests_H_
