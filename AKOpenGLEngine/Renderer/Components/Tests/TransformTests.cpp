//
//  TransformTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <yaml-cpp/yaml.h>

#include "TransformTests.h"
#include "../Transform.h"

using namespace glm;
using namespace YAML;
using namespace std;

namespace AKOpenGLEngine {

    TEST_F(TransformTests, CreatingTransform) {
        ASSERT_NE(nullptr, transform);
        ASSERT_EQ("Transform", transform->GetName());
    }

    TEST_F(TransformTests, TransformPosition) {
        vec3 pos = vec3(1.0f, -2.0f, 3.0f);
        transform->setPosition(pos);
        EXPECT_FLOAT_EQ(pos.x, transform->getPosition().x);
        EXPECT_FLOAT_EQ(pos.y, transform->getPosition().y);
        EXPECT_FLOAT_EQ(pos.z, transform->getPosition().z);
    }

    TEST_F(TransformTests, TransformScale) {
        vec3 scale = vec3(1.0f, -2.0f, 3.0f);
        transform->setScale(scale);
        EXPECT_FLOAT_EQ(scale.x, transform->getScale().x);
        EXPECT_FLOAT_EQ(scale.y, transform->getScale().y);
        EXPECT_FLOAT_EQ(scale.z, transform->getScale().z);
    }

    TEST_F(TransformTests, TransformRotationEuler) {
        vec3 rotation = vec3(M_PI_2, M_PI_4, -M_PI_4);
        transform->setRotation(rotation);
        EXPECT_FLOAT_EQ(rotation.x, transform->getRotationInEulerAngles().x);
        EXPECT_FLOAT_EQ(rotation.y, transform->getRotationInEulerAngles().y);
        EXPECT_FLOAT_EQ(rotation.z, transform->getRotationInEulerAngles().z);
    }

    TEST_F(TransformTests, TransformRotationQuaternion) {
        quat rotation = quat(0.70710678f, 0, -0.5f, 0.5);
        transform->setRotation(rotation);
        EXPECT_FLOAT_EQ(rotation.x, transform->getRotation().x);
        EXPECT_FLOAT_EQ(rotation.y, transform->getRotation().y);
        EXPECT_FLOAT_EQ(rotation.z, transform->getRotation().z);
        EXPECT_FLOAT_EQ(rotation.w, transform->getRotation().w);
    }

    TEST_F(TransformTests, TransformYAMLSerialization) {
        YAML::Emitter out;

        vec3 pos = vec3(1.0f, 2.0f, -3.0f);
        vec3 scale = vec3(1.0f, -2.0f, 3.0f);
        quat rotation = quat(0.70710678f, 0, -0.5f, 0.5);

        transform->setPosition(pos);
        transform->setScale(scale);
        transform->setRotation(rotation);
        out << transform;
        string serializedTransform = out.c_str();
        YAML::Node transformNode = YAML::Load(serializedTransform);
        Transform *transform1 = new Transform(transformNode);

        EXPECT_FLOAT_EQ(pos.x, transform1->getPosition().x);
        EXPECT_FLOAT_EQ(pos.y, transform1->getPosition().y);
        EXPECT_FLOAT_EQ(pos.z, transform1->getPosition().z);

        EXPECT_FLOAT_EQ(scale.x, transform1->getScale().x);
        EXPECT_FLOAT_EQ(scale.y, transform1->getScale().y);
        EXPECT_FLOAT_EQ(scale.z, transform1->getScale().z);

        EXPECT_FLOAT_EQ(rotation.x, transform1->getRotation().x);
        EXPECT_FLOAT_EQ(rotation.y, transform1->getRotation().y);
        EXPECT_FLOAT_EQ(rotation.z, transform1->getRotation().z);
        EXPECT_FLOAT_EQ(rotation.w, transform1->getRotation().w);

        delete transform1;
    }

    void TransformTests::SetUp() {
        Test::SetUp();
        transform = new Transform();
        ASSERT_NE(nullptr, transform);
    }

    void TransformTests::TearDown() {
        Test::TearDown();
        delete transform;
    }

}