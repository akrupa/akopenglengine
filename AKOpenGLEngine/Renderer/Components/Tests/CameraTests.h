//
//  CameraTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 13.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __CameraTests_H_
#define __CameraTests_H_

#include "../../../gtest/gtest.h"

namespace AKOpenGLEngine {

	class Camera;

	class CameraTests : public ::testing::Test {

    protected:
        Camera* camera;
        virtual void SetUp();
        virtual void TearDown();
	};

}


#endif // __CameraTests_H_
