//
//  Transform.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Transform_H_
#define __Transform_H_

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "Component.h"

namespace AKOpenGLEngine {

    class Transform : public Component {

#pragma mark Serialized values

        glm::vec3 position;
        glm::vec3 scale;
        glm::quat rotation;
        glm::vec3 rotationInEulerAngles;

#pragma mark Mutable values

        mutable bool transformChanged;
        mutable bool modelMatrixChanged;
        mutable int localFrameHash;
        mutable glm::mat4 cachedModelMatrix;
        mutable glm::mat4 cachedNormalMatrix;
        mutable glm::mat4 cachedViewMatrix;

#pragma mark Private methods
        void recomputeMatrices() const;

    public:

#pragma mark Constructors

        Transform();

        Transform(YAML::Node node);

#pragma mark Getters

        glm::vec3 getPosition() const;

        glm::vec3 getScale() const;

        glm::quat getRotation() const;

        glm::vec3 getRotationInEulerAngles() const;

        glm::mat4 getModelMatrix() const;

        glm::mat4 getNormalMatrix() const;

        glm::mat4 getViewMatrix() const;

#pragma mark Setters

        void setPosition(glm::vec3 position);

        void setScale(glm::vec3 scale);

        void setRotation(glm::quat rotation);

        void setRotation(glm::vec3 eulerAngles);

#pragma mark Virtual methods

        virtual std::string GetName() const override final;

        virtual void printYaml(YAML::Emitter &out) const override;

    };
}

#endif //__Transform_H_
