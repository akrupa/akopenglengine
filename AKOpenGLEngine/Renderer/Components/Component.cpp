//
//  Component.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>
#include <yaml-cpp/yaml.h>

#include "Component.h"
#include "../GameObject.h"
#include "Camera.h"

using namespace YAML;

namespace AKOpenGLEngine {

    Component::Component(GameObject *gameObject) {
        this->gameObject = gameObject;
    }

    Component::Component() {
        gameObject = nullptr;
        isEnabled = true;
    }

    void Component::Start() {
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
    }

    void Component::Update(float dt) {
        (void) dt;
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
        if (!isEnabled)
            return;
    }

    void Component::FixedUpdate(float dt) {
        (void) dt;
        if (gameObject == nullptr) {
            LOG(ERROR) << "Component " << GetName() << " has no game object";
        }
        if (!isEnabled)
            return;
    }

    void Component::AssignGameObject(GameObject *gameObject) {
        if (this->gameObject == nullptr) {
            this->gameObject = gameObject;
            Start();
        } else {
            this->gameObject = gameObject;
        }
    }

    bool Component::IsEnabled() {
        return isEnabled;
    }

    void Component::SetActive(bool active) {
        isEnabled = active;
    }

    Component::~Component() {

    }

    void Component::Render(unsigned int renderPass, Camera *camera, bool useLight ) {
        (void) renderPass;
        (void) camera;
        (void) useLight;
    }

    Transform *Component::GetTransform() const {
        return gameObject->GetTransform();
    }

    void Component::printYaml(Emitter &out) const {
        out << Key << "name" << Value << GetName();
        out << Key << "id" << Value << (long long int) this;
    }

    Emitter &operator<<(Emitter &out, const Component *c) {
        out << BeginMap;
        c->printYaml(out);
        out << EndMap;
        return out;
    }
}