//
//  MeshRenderer.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 25.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __MeshRenderer_H_
#define __MeshRenderer_H_

#include <GL/glew.h>
#include <vector>

#include "Component.h"

namespace YAML {
    class Node;
}

namespace AKOpenGLEngine {

    class Mesh;
    class Material;

    class MeshRenderer : public Component {

    private:

        GLuint m_VAO;

        GLuint m_indexBuffer;
        GLuint m_vertexBuffer;

        Mesh *mesh;
        std::vector<Material *>materials;

        GLuint allocatedIndexBuffer = 0;
        GLuint allocatedVertexBuffer = 0;

        char meshHash;
        static unsigned int renderPassHash;

        void FillData();
        void GenerateBuffers();
        void SetDepthParameters(unsigned int renderPass);
    public:
        virtual std::string GetName() const override final;
        MeshRenderer(Mesh* mesh, Material* material);
        MeshRenderer(Mesh* mesh);
        MeshRenderer(YAML::Node node);
        void* GetMaterial() const;
        void AddMaterial(Material* material);
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight) override;
        virtual void printYaml(YAML::Emitter& out) const override;

    };

}


#endif //__Mesh_H_
