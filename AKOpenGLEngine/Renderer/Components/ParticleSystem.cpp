//
//  ParticleSystem.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 20.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <algorithm>
#include <glm/gtc/random.hpp>

#include "ParticleSystem.h"
#include "Camera.h"
#include "../Materials/Material.h"
#include "../Utilities/GLDebug.h"
#include "../Materials/Shaders/ParticleAttrib.h"

using namespace std;
using namespace glm;
using namespace YAML;

namespace AKOpenGLEngine {

    const GLfloat ParticleSystem::billboardData[12];

    std::string ParticleSystem::GetName() const {
        return "ParticleSystem";
    }

    void ParticleSystem::Render(unsigned int renderPass, Camera *camera, bool useLight) {
        Component::Render(renderPass, camera, useLight);

        if (material == nullptr) {
            return;
        }
        if (useLight != material->isUsingLight()) {
            return;
        }

        mat4 VP = camera->getVPMatrix();
        mat4 ViewMatrix = camera->getViewMatrix();

        vec4 CameraRightWorldSpace = vec4(ViewMatrix[0][0], ViewMatrix[1][0], ViewMatrix[2][0],1);
        vec4 CameraUpWorldspace = vec4(ViewMatrix[0][1], ViewMatrix[1][1], ViewMatrix[2][1],1);

        material->SetMatrix("VPMatrix", VP, false, true);
        material->SetVector("CameraRightWorldSpace", CameraRightWorldSpace, false, true);
        material->SetVector("CameraUpWorldspace", CameraUpWorldspace, false, true);
        material->ApplyMaterial();

        GL_CHECK(glBindVertexArray(m_VAO));

        FillData();

        GL_CHECK(glDisable(GL_CULL_FACE));
        GL_CHECK(glDisable(GL_DEPTH_TEST));
        GL_CHECK(glEnable(GL_BLEND));
        GL_CHECK(glBlendFunc(GL_ONE, GL_ONE));

        GL_CHECK(glVertexAttribDivisor(ParticleAttribVertex, 0));
        GL_CHECK(glVertexAttribDivisor(ParticleAttribPosition, 1));

        GL_CHECK(glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, (GLsizei)particles[0].size()));
        material->ReleaseMaterial();
    }

    void ParticleSystem::printYaml(Emitter &out) const {
        Component::printYaml(out);
    }

    void ParticleSystem::SpawnParticle() {
        particlesToSpawnCounter--;

        Particle p;
        VertexParticleSystem p2;

        p.position = vec3(0);
        //p.position = vec3(linearRand(0.0f, 50.0f), linearRand(0.0f, 50.0f), linearRand(0.0f, 50.0f));
        p.position = vec3(gaussRand(0.0f, 3.0f),gaussRand(0.0f, 3.0f),gaussRand(0.0f, 3.0f));
        //p.velocity = vec3(gaussRand(0.0f, 0.01f), gaussRand(0.0f, 0.01f), gaussRand(0.0f, 0.01f));
        p.velocity = vec3(-p.position.y*0.00001f, p.position.x*0.00001f,0);
        //p.velocity = vec3(0);
        p.force = vec3(0);
        //p.mass = linearRand(5.0f, 20.0f);
        p.mass = 1;
        p.lifeTime = 0;
        for (unsigned int i = 0; i < particlesHistory; ++i) {
            particles[i].push_back(p);
        }

        p2.position = p.position;
        gpuParticles.push_back(p2);
    }

    void ParticleSystem::FixedUpdate(float dt) {

        UpdateParticles(dt);

        if (particles[0].size() < maxParticles) {
            particlesToSpawnCounter += particlesEmmisionRate * dt;
            particlesToSpawnCounter = glm::min((float) (maxParticles - particles[0].size()), particlesToSpawnCounter);
            while (particlesToSpawnCounter >= 1) {
                SpawnParticle();
            }
        }

        currentVectorParticlesCounter++;
    }

    ParticleSystem::ParticleSystem(Material * material) {
        this->material = material;
        GenerateBuffers();
    }

    void ParticleSystem::GenerateBuffers() {
        GL_CHECK(glGenVertexArrays(1, &m_VAO));
        GL_CHECK(glBindVertexArray(m_VAO));

        GL_CHECK(glGenBuffers(1, &billboardBuffer));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, billboardBuffer));
        GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(billboardData), billboardData, GL_STATIC_DRAW));

        GL_CHECK(glEnableVertexAttribArray(ParticleAttribVertex));
        GL_CHECK(glVertexAttribPointer(ParticleAttribVertex, 3, GL_FLOAT, GL_FALSE, 0, (void *)0));


        GL_CHECK(glGenBuffers(1, &particlesBuffer));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, particlesBuffer));

        GL_CHECK(glEnableVertexAttribArray(ParticleAttribPosition));
        GL_CHECK(glVertexAttribPointer(ParticleAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexParticleSystem), (void *) offsetof(VertexParticleSystem, position)));

        GL_CHECK(glBindVertexArray(0));
    }

    void ParticleSystem::FillData() {

        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, particlesBuffer));
        uint vertexDataCount = (uint)(gpuParticles.size()*sizeof(VertexParticleSystem));
        if (vertexDataCount != allocatedVertexBuffer) {
            allocatedVertexBuffer = vertexDataCount;
            GL_CHECK(glBufferData(GL_ARRAY_BUFFER, vertexDataCount, gpuParticles.data(), GL_STREAM_DRAW));
        } else {
            GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, 0, allocatedVertexBuffer, gpuParticles.data()));
        }
    }

    std::vector<Particle> &ParticleSystem::GetParticles() {
        return getParticles(0);
    }

    std::vector<Particle> &ParticleSystem::getParticles(unsigned int history) {
        return particles[(currentVectorParticlesCounter+history)%particlesHistory];
    }

    void ParticleSystem::UpdateParticles(float dt) {
        vector<Particle> &currentParticles = getParticles(0);

        for (unsigned long i = 0; i < currentParticles.size(); ++i) {
            auto particle = currentParticles[i];
            currentParticles[i]= UpdateParticleEulerODE(particle, dt);
            gpuParticles[i].position = particle.position;
        }
    }

    Particle ParticleSystem::UpdateParticleEulerODE(Particle p, float dt) {
        p.position += p.velocity*dt;
        p.velocity += (p.force/p.mass)*dt;
        return p;
    }
}
