//
//  MeshRenderer.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 25.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "MeshRenderer.h"
#include "../Materials/Meshes/Mesh.h"
#include "../Materials/Material.h"
#include "../Materials/Shaders/VertexAttrib.h"
#include "Camera.h"
#include "Transform.h"
#include "../Utilities/GLDebug.h"
#include "../Managers/ReferenceManager.h"
#include "../Managers/RendererManager.h"
#include "../Materials/Shaders/Shader.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {

    unsigned int MeshRenderer::renderPassHash;

    string MeshRenderer::GetName() const {
        return "MeshRenderer";
    }

    MeshRenderer::MeshRenderer(Mesh *mesh, Material *material) {
        this->mesh = mesh;
        this->meshHash = 0;
        materials.push_back(material);

        GenerateBuffers();
        MeshRenderer::renderPassHash = INT32_MAX;
        meshHash = mesh->GetMeshChangeHash() - (char) 1;
    }

    MeshRenderer::MeshRenderer(Mesh *mesh) {
        this->mesh = mesh;
        this->meshHash = 0;
        MeshRenderer::renderPassHash = INT32_MAX;
        GenerateBuffers();
    }

    MeshRenderer::MeshRenderer(YAML::Node node) {
        Mesh *mesh = (Mesh *) ReferenceManager::getInstance().GetReference(node["mesh"].as<long long int>());
        Material *material = (Material *) ReferenceManager::getInstance().GetReference(node["material"].as<long long int>());

        new(this)MeshRenderer(mesh, material);
    }

    void MeshRenderer::Render(unsigned int renderPass, Camera *camera, bool useLight) {
        Component::Render(renderPass, camera, useLight);

        if (renderPass >= materials.size()) {
            return;
        }
        Material *material = materials[renderPass];
        if (mesh == nullptr || material == nullptr) {
            return;
        }
        if (useLight != material->isUsingLight()) {
            return;
        }

        mat4 VP = camera->getVPMatrix();
        mat4 ProjectionMatrix = camera->getProjectionMatrix();
        mat4 ModelMatrix = GetTransform()->getModelMatrix();
        mat4 NormalMatrix = GetTransform()->getNormalMatrix();
        mat4 ViewMatrix = camera->getViewMatrix();
        mat4 ModelViewMatrix = ViewMatrix * ModelMatrix;

        material->SetMatrix("MVPMatrix", VP * ModelMatrix, false, true);
        material->SetMatrix("NormalMatrix", NormalMatrix, false, true);
        material->SetMatrix("ProjectionMatrix", ProjectionMatrix, false);
        material->SetMatrix("ModelViewMatrix", ModelViewMatrix, false, true);
        material->ApplyMaterial();

        GL_CHECK(glBindVertexArray(m_VAO));
        if (meshHash != mesh->GetMeshChangeHash()) {
            meshHash = mesh->GetMeshChangeHash();
            FillData();
        }

        if (useLight) {
            GL_CHECK(glBindBufferBase(GL_UNIFORM_BUFFER, (GLuint) material->GetShader()->getUniformLocation("Lights"), RendererManager::getInstance().getLightsBufferObject()));
        }

        SetDepthParameters(renderPass);
        GL_CHECK(glDrawElements(mesh->GetDrawType(), mesh->GetIndexDrawSize(), GL_UNSIGNED_INT, 0));
        material->ReleaseMaterial();
    }

    void MeshRenderer::FillData() {
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer));
        GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer));
        uint vertexDataCount = mesh->GetVertexDataSize();
        if (vertexDataCount != allocatedVertexBuffer) {
            allocatedVertexBuffer = vertexDataCount;
            GL_CHECK(glBufferData(GL_ARRAY_BUFFER, vertexDataCount, mesh->GetVertexData(), GL_DYNAMIC_DRAW));
        } else {
            GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, 0, allocatedVertexBuffer, mesh->GetVertexData()));
        }

        uint indexDataCount = mesh->GetIndexDataSize();
        if (indexDataCount != allocatedIndexBuffer) {
            allocatedIndexBuffer = indexDataCount;
            GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, allocatedIndexBuffer, mesh->GetIndexData(), GL_DYNAMIC_DRAW));
        } else {
            GL_CHECK(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, allocatedIndexBuffer, mesh->GetIndexData()));
        }
    }

    void *MeshRenderer::GetMaterial() const {
        return materials[0];
    }

    void MeshRenderer::printYaml(YAML::Emitter &out) const {
        Component::printYaml(out);
        out << YAML::Key << "mesh" << YAML::Value << (long long int) mesh;
        out << YAML::Key << "materials";
        out << YAML::BeginMap;
        for (unsigned long i = 0; i < materials.size(); ++i) {
            Material *material = materials[i];
            out << YAML::Key << i << YAML::Value << (long long int) material;
            ReferenceManager::getInstance().RegisterMaterialReference(material);
        }
        out << YAML::EndMap;
        ReferenceManager::getInstance().RegisterMeshReference(mesh);
    }

    void MeshRenderer::GenerateBuffers() {
        GL_CHECK(glGenVertexArrays(1, &m_VAO));
        GL_CHECK(glBindVertexArray(m_VAO));
        GL_CHECK(glGenBuffers(1, &m_vertexBuffer));
        GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer));
        GL_CHECK(glGenBuffers(1, &m_indexBuffer));
        GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer));

        GL_CHECK(glEnableVertexAttribArray(VertexAttribPosition));
        GL_CHECK(glVertexAttribPointer(VertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, position)));

        GL_CHECK(glEnableVertexAttribArray(VertexAttribNormal));
        GL_CHECK(glVertexAttribPointer(VertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, normal)));

        GL_CHECK(glEnableVertexAttribArray(VertexAttribTangent));
        GL_CHECK(glVertexAttribPointer(VertexAttribTangent, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, tangent)));

        GL_CHECK(glEnableVertexAttribArray(VertexAttribColor));
        GL_CHECK(glVertexAttribPointer(VertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, color)));

        GL_CHECK(glEnableVertexAttribArray(VertexAttribUV));
        GL_CHECK(glVertexAttribPointer(VertexAttribUV, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, UV)));

        GL_CHECK(glBindVertexArray(0));
    }

    void MeshRenderer::SetDepthParameters(unsigned int renderPass) {
        if(renderPass == MeshRenderer::renderPassHash)
            return;
        MeshRenderer::renderPassHash = renderPass;
        GL_CHECK(glEnable(GL_CULL_FACE));
        //GL_CHECK(glEnable(GL_MULTISAMPLE));
        GL_CHECK(glCullFace(GL_BACK));
        GL_CHECK(glEnable(GL_DEPTH_TEST));
        if (renderPass == 0) {
            GL_CHECK(glDepthMask(GL_TRUE));
            GL_CHECK(glDepthFunc(GL_LEQUAL));
            GL_CHECK(glDisable(GL_BLEND));
        } else {
            GL_CHECK(glDepthMask(GL_FALSE));
            GL_CHECK(glDepthFunc(GL_EQUAL));
            GL_CHECK(glEnable(GL_BLEND));
            GL_CHECK(glBlendFunc(GL_ONE, GL_ONE));
        }

    }

    void MeshRenderer::AddMaterial(Material *material) {
        materials.push_back(material);
    }
}
