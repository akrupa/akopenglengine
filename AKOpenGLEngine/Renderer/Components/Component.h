//
//  Component.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#ifndef __Component_H_
#define __Component_H_

#include <string>

namespace YAML {
    class Emitter;
    class Node;
}

namespace AKOpenGLEngine {

    class GameObject;
    class Transform;
    class Camera;
    class Light;

    class Component {

    protected:

        bool isUnique = false;
        bool isEnabled;
        GameObject *gameObject;

        Component();
        Component(GameObject* gameObject);

    public:
        virtual ~Component();
        virtual std::string GetName() const = 0;
        virtual void printYaml(YAML::Emitter& out) const;
        virtual void Start();
        virtual void Update(float dt);
        virtual void FixedUpdate(float dt);
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight);

        void AssignGameObject(GameObject* gameObject);
        bool IsEnabled();
        void SetActive(bool active);
        Transform* GetTransform() const ;

        friend YAML::Emitter& operator << (YAML::Emitter& out, const Component* c);
    };
}

#endif //__Component_H_
