//
//  NBodyParticleSystem.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 11.05.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __NBodyParticleSystem_H_
#define __NBodyParticleSystem_H_

#include "ParticleSystem.h"

namespace AKOpenGLEngine {

    class NBodyParticleSystem : public ParticleSystem {

    public:
        virtual std::string GetName() const override;
        virtual void Render(unsigned int renderPass, Camera *camera, bool useLight) override;
        virtual void printYaml(YAML::Emitter& out) const override;
        virtual void FixedUpdate(float dt) override;
        NBodyParticleSystem(Material *material);
    };
}


#endif //__NBodyParticleSystem_H_
