//
//  RendererManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 05.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <string>
#include <glm/glm.hpp>

#include "RendererManager.h"
#include "../Utilities/GLDebug.h"
#include "../Materials/Shaders/Miscellaneous/RendererManagerShader.h"
#include "../Components/Camera.h"

using namespace std;
using namespace glm;

namespace AKOpenGLEngine {

    string vertexShader = "#version 410 core \nvoid main(void) { gl_Position = vec4(0.0f);}";
    string fragmentShader = "#version 410 core\n"
            "layout(location = 0) out vec4 out_Color;\n"
            "struct Light {\n"
            "    vec4 typeRangeSpotAngleIntensity;\n"
            "    vec4 direction;\n"
            "    vec4 position;\n"
            "    vec4 color;\n"
            "};\n"
            "\n"
            "layout (shared) uniform Lights {\n"
            "    int numberOfLights;\n"
            "    Light light[4];\n"
            "};\n"
            "\n"
            "void main() {\n"
            "out_Color = vec4(numberOfLights);\n"
            "}";

    RendererManager::RendererManager() {

        RendererManagerShader *shader = new RendererManagerShader();
        shader->setup(vertexShader, fragmentShader);
        GL_CHECK(glUseProgram(shader->getProgram()));

        GL_CHECK_RETURN(GLuint blockIndex = glGetUniformBlockIndex(shader->getProgram(), "Lights"));

        GL_CHECK(glGetActiveUniformBlockiv(shader->getProgram(), blockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &lightsBlockSize));

        lightsBlockBuffer= (GLubyte *) malloc(lightsBlockSize);

        // Query for the offsets of each block variable
        const GLchar *names[MAX_LIGHTS_NUMBER*4+1];
        names[0] = "numberOfLights";
        string s1 = "light[";
        string s2 = "].";
        for(int i=0; i< MAX_LIGHTS_NUMBER; i++) {
            names[i*4+0+1] = new char [64];
            strcpy((char*)names[i*4+0+1], (s1+to_string(i)+s2+"typeRangeSpotAngleIntensity").c_str());
            names[i*4+1+1] = new char [32];
            strcpy((char*)names[i*4+1+1], (s1+to_string(i)+s2+"direction").c_str());
            names[i*4+2+1] = new char [32];
            strcpy((char*)names[i*4+2+1], (s1+to_string(i)+s2+"position").c_str());
            names[i*4+3+1] = new char [32];
            strcpy((char*)names[i*4+3+1], (s1+to_string(i)+s2+"color").c_str());
        }

        GLuint indices[MAX_LIGHTS_NUMBER*4+1];
        GL_CHECK(glGetUniformIndices(shader->getProgram(), MAX_LIGHTS_NUMBER*4+1, names, indices));

        lightBlockOffsets = new GLint[MAX_LIGHTS_NUMBER*4+1];
        GL_CHECK(glGetActiveUniformsiv(shader->getProgram(), MAX_LIGHTS_NUMBER*4+1, indices, GL_UNIFORM_OFFSET, lightBlockOffsets));

        GL_CHECK(glGenBuffers(1, &lightsBufferObject));
        GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, lightsBufferObject));
        GL_CHECK(glBufferData(GL_UNIFORM_BUFFER, lightsBlockSize, NULL, GL_DYNAMIC_DRAW));
        GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, 0));

        for(int i=0; i< MAX_LIGHTS_NUMBER; i++) {
            delete names[i*4+0+1];
            delete names[i*4+1+1];
            delete names[i*4+2+1];
            delete names[i*4+3+1];
        }
        delete shader;
    }

    GLuint RendererManager::getLightsBufferObject() {
        return lightsBufferObject;
    }

    void RendererManager::prepareLights(std::vector<LightStruct> lights, Camera *camera) {
        int numberOfLights = glm::min<int>(4, (int)lights.size());
        memcpy(lightsBlockBuffer, &numberOfLights, sizeof(int));

        for (int i = 0; i < numberOfLights; i++) {
            vec4 data;
            data = vec4(lights[i].type, lights[i].range, lights[i].spotAngle, lights[i].intensity);
            memcpy(lightsBlockBuffer+lightBlockOffsets[4*i+0+1], &data, sizeof(vec4));
            data = vec4(lights[i].direction, 1);
            data = data * camera->getModelMatrix(); //rotate light to eye coorde
            memcpy(lightsBlockBuffer+lightBlockOffsets[4*i+1+1], &data, sizeof(vec4));
            data = vec4(lights[i].position, 1);
            data = camera->getViewMatrix() * data; //move light to eye coorde
            memcpy(lightsBlockBuffer+lightBlockOffsets[4*i+2+1], &data, sizeof(vec4));
            data = vec4(lights[i].color, 1);
            memcpy(lightsBlockBuffer+lightBlockOffsets[4*i+3+1], &data, sizeof(vec4));
        }

        GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, lightsBufferObject));
        GL_CHECK(glBufferSubData(GL_UNIFORM_BUFFER, 0, lightsBlockSize, lightsBlockBuffer));

    }

    void RendererManager::startRenderingWithCamera(Camera *camera) {
        currentCamera = camera;
    }


    Camera *RendererManager::getCurrentCamera() {
        return currentCamera;
    }

    void RendererManager::startNewFrame() {
        frameHash = rand();
    }

    int RendererManager::getFrameHash() {
        return frameHash;
    }
}
