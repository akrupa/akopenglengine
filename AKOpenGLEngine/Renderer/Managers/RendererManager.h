//
//  RendererManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 05.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __RendererManager_H_
#define __RendererManager_H_

#include <GL/glew.h>
#include <vector>

#include "../Utilities/Singleton.h"
#include "../Components/Light.h"

namespace AKOpenGLEngine {

    class Camera;

    class RendererManager : public Singleton<RendererManager> {

        GLuint lightsBufferObject;
        GLubyte * lightsBlockBuffer;
        GLint lightsBlockSize;
        GLint *lightBlockOffsets;

        int frameHash;

        Camera *currentCamera;

    public:
        RendererManager();

        GLuint getLightsBufferObject();
        void prepareLights(std::vector<LightStruct> lights, Camera* camera);
        void startNewFrame();
        void startRenderingWithCamera(Camera *camera);
        int getFrameHash();
        Camera *getCurrentCamera();
    };
}


#endif //__RendererManager_H_
