//
//  ModelManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 03.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __ModelManager_H_
#define __ModelManager_H_

#include <string>

namespace AKOpenGLEngine {

    class Mesh;

    class ModelManager {

    ModelManager();

    public:
        static Mesh *LoadMesh(std::string filename);

    };
}


#endif //__ModelManager_H_
