//
//  InputManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "InputManager.h"

using namespace glm;

namespace AKOpenGLEngine {

    GLFWwindow* InputManager::window;

    InputManager::InputManager() {
    }

    void InputManager::RegisterWindow(GLFWwindow *window) {
        InputManager::window = window;
    }

    vec2 InputManager::GetMousePosition() {
        double xPos, yPos;
        glfwGetCursorPos(window, &xPos, &yPos);
        return vec2(xPos, yPos);
    }

    bool InputManager::GetMouseButtonState(int key) {
        int state = glfwGetMouseButton(window, key);
        return state == GLFW_PRESS || state == GLFW_REPEAT;
    }

    bool InputManager::GetKeyboardButtonState(int button) {
        int state = glfwGetKey(window, button);
        return state == GLFW_PRESS;
    }
}
