//
//  TextureManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 01.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __TextureManager_H_
#define __TextureManager_H_

#include <GL/glew.h>
#include <map>
#include <string>

#include "../Utilities/Singleton.h"

namespace YAML {
    class Node;
};

namespace AKOpenGLEngine {

    class Texture;

    class TextureManager : public Singleton<TextureManager> {

        std::map<unsigned int, GLuint> m_texID;

    public:
        Texture* LoadTexture(std::string filename,    //where to load the file from
                GLenum image_format = GL_RGB,           //format the image is in
                GLenum internal_format = GL_RGB,         //format to store the image in
                GLint level = 0,                        //mipmapping level
                GLint border = 0);                      //border size

        Texture* LoadTexture(YAML::Node textureNode);

        bool UnloadTexture(const unsigned int texID);

        bool BindTexture(const Texture *tex);

        void UnloadAllTextures();

        void SaveTextureToPNG(std::string filename,
                int width,
                int height,
                unsigned int framebuffer);
    };
}


#endif //__TextureManager_H_
