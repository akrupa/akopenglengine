//
//  InputManager.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 22.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __InputManager_H_
#define __InputManager_H_

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

namespace AKOpenGLEngine {

    class InputManager {
        static GLFWwindow *window;

        InputManager();

    public:
        static void RegisterWindow(GLFWwindow *window);
        static glm::vec2 GetMousePosition();
        static bool GetMouseButtonState(int button);
        static bool GetKeyboardButtonState(int button);
    };
}


#endif //__InputManager_H_
