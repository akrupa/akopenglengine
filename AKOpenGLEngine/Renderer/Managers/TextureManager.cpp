//
//  TextureManager.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 01.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <easylogging++/easylogging++.h>
#include <FreeImage/FreeImage.h>
#include <yaml-cpp/yaml.h>

#include "TextureManager.h"
#include "../Utilities/GLDebug.h"
#include "../Utilities/WorkingDirectory.h"
#include "../Materials/Texture.h"

using namespace std;
using namespace YAML;

namespace AKOpenGLEngine {

    Texture *TextureManager::LoadTexture(string filename, GLenum image_format, GLenum internal_format, GLint level, GLint border) {

        FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
        FIBITMAP *dib(0);
        BYTE *bits(0);
        Texture *texture = nullptr;
        unsigned int width(0), height(0);
        GLuint gl_texID;
        string file = WorkingDirectory::GetExecutableDirectory() + "/resources/textures/" + filename;

        fif = FreeImage_GetFileType(file.c_str(), 0);
        if (fif == FIF_UNKNOWN) {
            fif = FreeImage_GetFIFFromFilename(file.c_str());
        }
        if (fif == FIF_UNKNOWN) {
#if !defined(_TEST)
            LOG(ERROR) << "Unknown texture type " << file;
#endif
            return texture;
        }
        if (FreeImage_FIFSupportsReading(fif)) {
            dib = FreeImage_Load(fif, file.c_str());
        }
        if (!dib) {
#if !defined(_TEST)
            LOG(ERROR) << "Couldn't load texture " << file;
#endif
            return texture;
        }

        bits = FreeImage_GetBits(dib);
        width = FreeImage_GetWidth(dib);
        height = FreeImage_GetHeight(dib);
        if ((bits == 0) || (width == 0) || (height == 0)) {
#if !defined(_TEST)
            LOG(ERROR) << "Couldn't load texture size " << file;
#endif
            return texture;
        }

        GL_CHECK(glGenTextures(1, &gl_texID));
        m_texID[gl_texID] = gl_texID;
        GL_CHECK(glBindTexture(GL_TEXTURE_2D, gl_texID));
        GL_CHECK(glTexImage2D(GL_TEXTURE_2D, level, internal_format, width, height,
                border, image_format, GL_UNSIGNED_BYTE, bits));

        GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
        GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));

        FreeImage_Unload(dib);

        texture = new Texture(gl_texID, filename, image_format, internal_format, level, border);

        return texture;
    }

    Texture *TextureManager::LoadTexture(Node textureNode) {
        string filename = textureNode["name"].as<string>();
        GLenum image_format = textureNode["image_format"].as<GLenum>();
        GLenum internal_format = textureNode["internal_format"].as<GLenum>();
        GLint level = textureNode["level"].as<GLint>();
        GLint border = textureNode["border"].as<GLint>();
        return LoadTexture(filename, image_format, internal_format, level, border);
    }

    bool TextureManager::UnloadTexture(const unsigned int texID) {
        bool result(true);
        if (m_texID.find(texID) != m_texID.end()) {
            glDeleteTextures(1, &(m_texID[texID]));
            m_texID.erase(texID);
        }
        else {
            result = false;
        }

        return result;
    }

    bool TextureManager::BindTexture(const Texture *tex) {
        bool result(true);
        unsigned int texID = (unsigned int) tex->GetBind();
        if (m_texID.find(texID) != m_texID.end()) {
            GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texID[texID]));
        } else {
            result = false;
        }

        return result;
    }

    void TextureManager::UnloadAllTextures() {
        map<unsigned int, GLuint>::iterator i = m_texID.begin();
        while (i != m_texID.end()) {
            UnloadTexture(i->first);
        }
        m_texID.clear();
    }

    void TextureManager::SaveTextureToPNG(std::string filename, int width, int height, unsigned int framebuffer) {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

        GLubyte *pixels = (GLubyte *) malloc(4 * width * height * sizeof(GLubyte));

        glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, pixels);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        FIBITMAP *Image = FreeImage_ConvertFromRawBits(pixels, width, height, 4 * width, 32, 0x0000FF, 0x00FF00, 0xFF0000, false);
        FreeImage_Save(FIF_PNG, Image, (WorkingDirectory::GetExecutableDirectory() + "/resources/textures/" + filename).c_str(), 0);
    }


}
