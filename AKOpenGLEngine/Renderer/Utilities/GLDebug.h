//
//  GLDebug.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 10.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __GLDebug_h
#define __GLDebug_h

#include <easylogging++/easylogging++.h>
#include <string>

#if defined(_DEBUG)
    #define \
        GL_CHECK(stmt)\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                std::stringstream stream;\
                stream << err;\
                LOG(ERROR)<<stream.str()<<" (previous GL call) "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
            stmt;\
            err = glGetError();\
            while((err = glGetError()) != GL_NO_ERROR) {\
                std::stringstream stream;\
                stream << err;\
                LOG(ERROR)<<stream.str()<<" "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while (0)

    #define \
        GL_CHECK_RETURN(stmt)\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                std::stringstream stream;\
                stream << err;\
                LOG(ERROR)<<stream.str()<<" (previous GL call) "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while(0);\
        stmt;\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR) {\
                std::stringstream stream;\
                stream << err;\
                LOG(ERROR)<<stream.str()<<" "<<#stmt<<" "<<el::base::debug::StackTrace();\
            }\
        } while(0);


    #define \
        GL_CHECK_CLEAR()\
        do {\
            GLenum err;\
            while((err = glGetError()) != GL_NO_ERROR);\
        } while(0);
#else
    #define GL_CHECK(stmt) stmt
    #define GL_CHECK_RETURN(stmt) stmt
    #define GL_CHECK_CLEAR() ;
#endif

#endif
