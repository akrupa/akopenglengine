//
//  WorkingDirectory.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 27.02.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __WorkingDirectory_H_
#define __WorkingDirectory_H_

#include <string>
#include <cstdlib>

namespace AKOpenGLEngine {

    class WorkingDirectory {
        static std::string argv0;
    public:
        static void SetArgv0(char *argv0);

        static std::string GetExecutableDirectory();
    };
}

#endif //__WorkingDirectory_H_
