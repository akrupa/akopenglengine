//
//  Renderer.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <iostream>
#include <easylogging++/easylogging++.h>

#include "Renderer.h"
#include "Scene.h"
#include "Managers/InputManager.h"
#include "Utilities/GLDebug.h"

using namespace std;
using namespace el;

INITIALIZE_EASYLOGGINGPP

namespace AKOpenGLEngine {

    Renderer* Renderer::instance;

    static void error_callback(int error, const char* description) {
        (void)error;
        fputs(description, stderr);
    }

    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
        (void)scancode;
        (void)mods;
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GL_TRUE);
    }

    void Renderer::window_size_callback(GLFWwindow* window, int width, int height) {
        (void)window;
        instance->currentScene->SetCamerasAspect(width,height);
    }

    void Renderer::Init() {
        instance = this;
        currentScene = new Scene();
        currentScene->InitBasicScene();

    }

    void Renderer::Draw() {
        renderFrames++;
        auto currentTime = glfwGetTime();
        auto elapsedTimeFromLastFPSLogTime = currentTime - lastFPSLogTime;
        auto deltaTime = currentTime - lastUpdateDrawTime;

        fixedDeltaTimeToUpdate += deltaTime;
        //while (fixedDeltaTimeToUpdate > fixedDeltaTime) {
            currentScene->FixedUpdate(1);
            fixedDeltaTimeToUpdate -= fixedDeltaTime;
        //}

        auto startUpdateTime = glfwGetTime();
        currentScene->Update((float) deltaTime);
        auto endUpdateTime = glfwGetTime();

        auto startRenderTime = glfwGetTime();
        currentScene->Render();
        auto endRenderTime = glfwGetTime();

        renderClock += endRenderTime - startRenderTime;
        updateClock += endUpdateTime - startUpdateTime;

        if (elapsedTimeFromLastFPSLogTime > 5) {
            printf("Frame render time: %lf ms\n", renderClock * 1000.0 / renderFrames);
            printf("Frame update time: %lf ms\n", updateClock * 1000.0 / renderFrames);
            printf("FPS: %lf\n", renderFrames / elapsedTimeFromLastFPSLogTime);
            renderFrames = 0;
            renderClock = 0;
            updateClock = 0;
            lastFPSLogTime = currentTime;
        }
        lastUpdateDrawTime = currentTime;
    }

    void Renderer::Stop() {
    }

    void Renderer::InitLogger() {
        el::Configurations defaultConf;
        defaultConf.setToDefault();
        defaultConf.setGlobally(el::ConfigurationType::Format, "%level %fbase::%line: %msg");
        el::Loggers::reconfigureLogger("default", defaultConf);
    }

    void Renderer::CreateOpenGLWindow() {

        glfwSetErrorCallback(error_callback);
		if (!glfwInit()) {
			LOG(ERROR) << "Couldn't init glfw";
			exit(EXIT_FAILURE);
		}
#if defined(__APPLE__)
        int minor = -1;
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
#else
        int minor = 5;
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
#endif
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_SAMPLES, 4);

        while(!window && minor + 1 >= 0) {
            window = glfwCreateWindow(960, 720, "AKOpenGLEngine", NULL, NULL);
            if (!window) {
            	minor--;
            	if(minor<0) {
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
            	} else {
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
            	}
            }
        }

        if (!window) {
			LOG(ERROR) << "Couldn't open glfw window";
            glfwTerminate();
            exit(EXIT_FAILURE);
        }

        glfwMakeContextCurrent(window);
        glfwSetKeyCallback(window, key_callback);
        glfwSetWindowSizeCallback(window, &Renderer::window_size_callback);
        //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        if (GLEW_OK != err)
        {
            fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        }

        InputManager::RegisterWindow(window);

        //const char* cmd = "ffmpeg -loglevel panic -r 60 -f rawvideo -pix_fmt rgba -s 960x720 -i - " " -threads 0 -preset fast -y -crf 21 -vf vflip output.mp4 >> /dev/null";
        //ffmpeg = popen(cmd, "r+");
        GL_CHECK_CLEAR();
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VENDOR));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_VERSION));
        GL_CHECK_RETURN(LOG(INFO) << glGetString(GL_RENDERER));
    }

    void Renderer::Run() {

        Init();

        while (!glfwWindowShouldClose(window)) {
            //gl::glClear((ClearBufferMask) GL_COLOR_BUFFER_BIT);
            Draw();

            glfwSwapInterval(0);

            glfwSwapBuffers(window);
/*
            int width = 960;
            int height = 720;
            int* buffer = new int[width*height];
            glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

            fwrite(buffer, sizeof(int)*width*height, 1, ffmpeg);
            */
            glfwPollEvents();
        }

        Stop();
        DestroyWindow();
        //pclose(ffmpeg);
    }

    void Renderer::CreateDebugContext() {
        if (!glfwInit())
            exit(EXIT_FAILURE);
#if defined(__APPLE__)
        int minor = -1;
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
#else
        int minor = 5;
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
#endif
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);

        while(!window && minor + 1 >= 0) {
            window = glfwCreateWindow(960, 720, "AKOpenGLEngine", NULL, NULL);
            if (!window) {
                minor--;
                if(minor<0) {
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
                } else {
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
                }
                glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
                glfwTerminate();
                if(minor == -2) {
                    throw new rendererException();
                }
            }
        }



        glfwMakeContextCurrent(window);
        glewExperimental = GL_TRUE;
        GLenum err = glewInit();
        glfwHideWindow(window);

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        if(width != 960 || height != 720) {
            glfwSetWindowSize(window, (int)(960 * 960.0f / width), (int)(720 * 720.0f / height));
        }
        if (GLEW_OK != err) {
            throw new rendererException();
        }
        GL_CHECK_CLEAR();
    }

    void Renderer::DestroyWindow() {
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void Renderer::DrawScene(Scene *scene) {
        Scene *s = currentScene;
        currentScene = scene;

        Draw();

        currentScene=s;
    }
}
