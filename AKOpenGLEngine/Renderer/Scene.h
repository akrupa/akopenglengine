//
//  Scene.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Scene_H_
#define __Scene_H_

#include <vector>

namespace AKOpenGLEngine {

    class Camera;
    class GameObject;
    class Light;

    class Scene {

        std::vector<Camera *> cameras;
        std::vector<Light *> lights;
        std::vector<GameObject *> gameObjects;

        void Render(Camera* camera);

    public:

        void ExportSceneToYaml();
        void ImportSceneFromYaml();

        void InitBasicScene();
        void Update(float dt);
        void FixedUpdate(float dt);
        void Render();
        void AddGameObject(GameObject *gameObject);

        void SetCamerasAspect(int width, int height);

    };
}


#endif //__Scene_H_
