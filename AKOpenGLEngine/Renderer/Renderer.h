//
//  Renderer.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __Renderer_H_
#define __Renderer_H_

#include <mutex>
#include <thread>
#include <exception>
#include <GL/glew.h>
#include <GLFW/glfw3.h>


namespace AKOpenGLEngine {

    class Scene;

    class rendererException: public std::exception {};

    class Renderer {
        GLFWwindow *window;
        //FILE* ffmpeg;

        Scene *currentScene;
        double lastFPSLogTime = 0;
        double fixedDeltaTimeToUpdate = 0;
        double renderClock = 0;
        double updateClock = 0;
        int renderFrames = 0;
        double lastUpdateDrawTime;
        float fixedDeltaTime = 1.0f/30.0f;

        static void window_size_callback(GLFWwindow* window, int width, int height);
        static Renderer* instance;

        void Init();
        void Draw();
        void Stop();

    public:
        void DrawScene(Scene *scene);
        void InitLogger();
		void CreateOpenGLWindow();
        void CreateDebugContext();
        void DestroyWindow();
        void Run();
    };
}


#endif //__Renderer_H_
