//
//  Scene.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 17.01.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <algorithm>
#include <iostream>
#include <easylogging++/easylogging++.h>
#include <yaml-cpp/yaml.h>

#include "Scene.h"
#include "Materials/Shaders/Shaders.h"
#include "Materials/Material.h"
#include "Materials/Meshes/ExampleMeshes/Meshes.h"
#include "Components/MeshRenderer.h"
#include "GameObject.h"
#include "Components/Camera.h"
#include "Components/Transform.h"
#include "Components/Light.h"
#include "Components/MyScripts/CameraMovement3D.h"
#include "Components/MyScripts/ObjectRotator.h"
#include "Components/MyScripts/NBodySimulator.h"
#include "Managers/ModelManager.h"
#include "Managers/TextureManager.h"
#include "Managers/ReferenceManager.h"
#include "Managers/RendererManager.h"
#include "Components/ParticleSystem.h"
#if !defined(__APPLE__)
#include <GL/glew.h>
#include "Utilities/GLDebug.h"
#endif

namespace AKOpenGLEngine {

    void Scene::Update(float dt) {
        for (auto gameObject : gameObjects) {
            gameObject->Update(dt);
        }
    }

    void Scene::FixedUpdate(float dt) {
        for (auto gameObject : gameObjects) {
            gameObject->FixedUpdate(dt);
        }
    }

    void Scene::Render() {
        RendererManager::getInstance().startNewFrame();
        for (auto camera : cameras) {
            if (camera == nullptr || !camera->IsEnabled()) {
                continue;
            }
            Render(camera);
        }
    }

    void Scene::InitBasicScene() {

        //ImportSceneFromYaml();
        //return;
        GameObject *camera = GameObject::CreateCamera();
        camera->GetTransform()->setPosition(glm::vec3(0, 0, 100));
        CameraMovement3D *cameraMovement3D = new CameraMovement3D();
        camera->AddComponent(cameraMovement3D);
        AddGameObject(camera);
        GameObject *light1 = GameObject::CreatePointLight();
        light1->GetTransform()->setPosition(glm::vec3(0,2,1.5));
        light1->GetTransform()->setRotation(glm::vec3(0.0f, 0.0f, -M_PI_2));
        ((Light *)light1->GetComponentOfType("Light"))->setLightSpotAngle(M_PI_4/2);
        AddGameObject(light1);

/*
        Shader *shader = Shaders::getInstance().GetAmbientShader();
        Shader *shader2 = Shaders::getInstance().GetDiffuseSpecularShader();
        Shader *shaderGeo = Shaders::getInstance().GetDebugNormalsShader();
        Texture *tex = TextureManager::getInstance().LoadTexture("brick.jpg", GL_BGR);
        Texture *tex2 = TextureManager::getInstance().LoadTexture("brick_norm.jpg", GL_BGR);
        Material *material = new Material(shader);
        Material *material2 = new Material(shader2);
        Material *materialGeo = new Material(shaderGeo);
        material2->SetTexture("texture0", tex);
        material2->SetTexture("textureNormal", tex2);
        material->SetColor("Color", glm::vec4(0.05, 0.05, 0.07, 1));
        material2->SetColor(glm::vec4(0, 0, 1, 1));
        Mesh *mesh = ModelManager::LoadMesh("scene.obj");
        //Mesh *mesh = Meshes::getInstance().GetCubeMesh();
        //Mesh *mesh2 = new TubeMesh();

        for (int i = 0; i < 1; i++) {
            //Mesh *mesh = new CubeMesh(glm::vec4((rand() % 256) / 256.0f, (rand() % 256) / 256.0f, (rand() % 256) / 256.0f, 1));
            GameObject *go = new GameObject();
            MeshRenderer *meshRenderer = new MeshRenderer(mesh);
            //ObjectRotator *rotator = new ObjectRotator();
            meshRenderer->AddMaterial(material);
            meshRenderer->AddMaterial(material2);
            go->AddComponent(meshRenderer);
            //go->AddComponent(rotator);
            glm::vec3 pos = glm::vec3(rand() % 200 - 100, rand() % 200 - 100, rand() % 200 - 100);
            pos = i==0?glm::vec3(0, 0, 0):pos;
            go->GetTransform()->setPosition(pos);
            //go->GetTransform()->setScale(glm::vec3(1,1,1));
            go->SetName("Point " + std::to_string(i));
            AddGameObject(go);
/*
            if (GL_EXT_geometry_shader4 || GL_ARB_geometry_shader4 || GLEW_ARB_geometry_shader4) {
                //Mesh *mesh2 = new CubeMesh(glm::vec4((rand() % 256) / 256.0f, (rand() % 256) / 256.0f, (rand() % 256) / 256.0f, 1));
                GameObject *go2 = new GameObject();
                MeshRenderer *meshRenderer2 = new MeshRenderer(mesh, materialGeo);
                go2->AddComponent(meshRenderer2);
                go2->GetTransform()->setPosition(pos);
                //ObjectRotator *rotator = new ObjectRotator();
                //go2->AddComponent(rotator);
                go2->SetName("Point " + std::to_string(i));
                AddGameObject(go2);
            } else {
                LOG(INFO)<<"Geometry shader is not available";
            }
*/
        //}


        Shader *particlesShader = Shaders::getInstance().GetUnlitParticlesTexturedShader();
        Texture *tex = TextureManager::getInstance().LoadTexture("flare.jpg", GL_BGR);
        Material *particleMaterial = new Material(particlesShader);
        particleMaterial->SetTexture("texture0", tex);
        ParticleSystem *ps = new ParticleSystem(particleMaterial);
        GameObject *particleSystemGO = new GameObject();
        particleSystemGO->AddComponent(ps);
        particleSystemGO->AddComponent(new NBodySimulator());
        AddGameObject(particleSystemGO);

    }

    void Scene::Render(Camera *camera) {
        RendererManager::getInstance().startRenderingWithCamera(camera);
        camera->Clear();
        //sort by materials
        sort(gameObjects.begin(), gameObjects.end(), [](GameObject *a, GameObject *b) {
            return *a < *b;
        });
        for (auto gameObject : gameObjects) {
            gameObject->Render(0, camera, false);
        }

        std::vector<LightStruct> lightsVec;
        for(auto it = lights.begin(); it != lights.end(); it++) {
            lightsVec.clear();
            for (int i = 0; i < MAX_LIGHTS_NUMBER; ++i) {
                lightsVec.push_back((*it)->getLightStruct());
                it++;
				if (it == lights.end())  {
					it--;
					break;
				}
            }
            if (lightsVec.size()>0) {
                RendererManager::getInstance().prepareLights(lightsVec, camera);
                for (auto gameObject : gameObjects) {
                    gameObject->Render(1, camera, true);
                }
            }
        }

    }

    void Scene::AddGameObject(GameObject *gameObject) {
        if (gameObject->HasComponentOfType("Camera")) {
            Camera *cam = dynamic_cast<Camera *>(gameObject->GetComponentOfType("Camera"));
            cameras.push_back(cam);
        }
        if (gameObject->HasComponentOfType("Light")) {
            Light *light = dynamic_cast<Light *>(gameObject->GetComponentOfType("Light"));
            lights.push_back(light);
        }
        gameObjects.push_back(gameObject);
    }

    void Scene::SetCamerasAspect(int width, int height) {
#if !defined(__APPLE__)
		GL_CHECK(glViewport(0, 0, width, height));
#endif
        float aspect = (float)width / (float)height;
        for (auto camera : cameras) {
            camera->setAspect(aspect);
        }
    }

    void Scene::ExportSceneToYaml() {
        std::ofstream ofstr("scene.yaml");
        YAML::Emitter out;
        out << YAML::BeginMap;
        out << YAML::Key << "game objects" << YAML::Value << gameObjects;
        ReferenceManager::getInstance().printYaml(out);
        out << YAML::EndMap;
        ofstr << out.c_str();
    }

    void Scene::ImportSceneFromYaml() {

        YAML::Node scene = YAML::LoadFile("scene.yaml");

        YAML::Node shaders = scene["Shaders"];
        for (YAML::const_iterator shadersIterator = shaders.begin(); shadersIterator != shaders.end(); ++shadersIterator) {
            Shader *shader = Shaders::getInstance().GetShader(((*shadersIterator)["name"]).as<std::string>());
            ReferenceManager::getInstance().RegisterReference(((*shadersIterator)["id"]).as<long long int>(), shader);
        }

        YAML::Node textures = scene["Textures"];
        for (YAML::const_iterator texturesIterator = textures.begin(); texturesIterator != textures.end(); ++texturesIterator) {
            Texture *tex = TextureManager::getInstance().LoadTexture(*texturesIterator);
            ReferenceManager::getInstance().RegisterReference(((*texturesIterator)["id"]).as<long long int>(), tex);
        }

        YAML::Node materials = scene["Materials"];
        for (YAML::const_iterator materialsIterator = materials.begin(); materialsIterator != materials.end(); ++materialsIterator) {
            Material *mat = new Material(*materialsIterator);
            ReferenceManager::getInstance().RegisterReference(((*materialsIterator)["id"]).as<long long int>(), mat);
        }

        YAML::Node meshes = scene["Meshes"];
        for (YAML::const_iterator meshesIterator = meshes.begin(); meshesIterator != meshes.end(); ++meshesIterator) {
            Mesh *mesh = Meshes::getInstance().GetMesh(((*meshesIterator)["name"]).as<std::string>());
            ReferenceManager::getInstance().RegisterReference(((*meshesIterator)["id"]).as<long long int>(), mesh);
        }

        YAML::Node gameObjects = scene["game objects"];
        for (YAML::const_iterator gameObjectsIterator = gameObjects.begin(); gameObjectsIterator != gameObjects.end(); ++gameObjectsIterator) {
            GameObject *go = new GameObject(*gameObjectsIterator);
            ReferenceManager::getInstance().RegisterReference(((*gameObjectsIterator)["id"]).as<long long int>(), go);
            AddGameObject(go);
        }
    }
}
