//
//  RenderingTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "RenderingTests.h"
#include "../Scene.h"
#include "../GameObject.h"
#include "../Components/Transform.h"
#include "../Components/MeshRenderer.h"
#include "../Managers/TextureManager.h"
#include "../Materials/Shaders/Shader.h"
#include "../Materials/Shaders/Shaders.h"
#include "../Materials/Material.h"
#include "../Materials/Meshes/Mesh.h"
#include "../Materials/Meshes/ExampleMeshes/Meshes.h"
#include "../Managers/ModelManager.h"


namespace AKOpenGLEngine {
    TEST_F(RenderingTests, TestScene) {
        InitScene();

        renderer->DrawScene(scene);
        printf("Save test scene \n");
        TextureManager::getInstance().SaveTextureToPNG("testScene.png", 960, 720, 0);
        scene->ExportSceneToYaml();
    }

    void RenderingTests::InitScene() {
        scene = new Scene();
        AddCamera();
        AddDirectionalLights(1);


        glm::vec3 pos = glm::vec3(0,-2,0);
        glm::vec3 rot = glm::vec3(0,0,0);

        for (int i = 0; i < 3; ++i) {
            Shader *ambientShader = Shaders::getInstance().GetAmbientShader();
            Shader *diffuseShader = Shaders::getInstance().GetDiffuseSpecularShader();
            Material *ambientMaterial = new Material(ambientShader);
            Material *diffuseMaterial = new Material(diffuseShader);
            ambientMaterial->SetColor("Color", glm::vec4(0.05, 0.05, 0.07, 1));
            diffuseMaterial->SetColor("Color", glm::vec4(0.05, 0.15, 0.70, 1));
            Mesh *cubeMesh = Meshes::getInstance().GetMesh("scene.obj");
            GameObject *go = new GameObject();
            MeshRenderer *cubeMeshRenderer = new MeshRenderer(cubeMesh);
            cubeMeshRenderer->AddMaterial(ambientMaterial);
            cubeMeshRenderer->AddMaterial(diffuseMaterial);
            go->AddComponent(cubeMeshRenderer);
            go->GetTransform()->setPosition(pos);
            go->GetTransform()->setRotation(rot);
            scene->AddGameObject(go);
            pos += glm::vec3(0, 1.7, 0);
            rot += glm::vec3(M_PI_4, 0, 0);
        }
    }

    void RenderingTests::AddCamera() {
        GameObject *camera = GameObject::CreateCamera();
        camera->GetTransform()->setPosition(glm::vec3(0, 0, 6));
        scene->AddGameObject(camera);
    }

    void RenderingTests::AddDirectionalLights(int lights) {
        if (lights >= 1) {
            GameObject *directionalLight = GameObject::CreateDirectionalLight();
            directionalLight->GetTransform()->setRotation(glm::vec3(0.0f, 0.0f, -M_PI_2));
            scene->AddGameObject(directionalLight);
        }
        if (lights >= 2) {

            GameObject *directionalLight2 = GameObject::CreateDirectionalLight();
            directionalLight2->GetTransform()->setRotation(glm::vec3(0.0f, 0.0f, 0.0f));
            scene->AddGameObject(directionalLight2);
        }
        if (lights >= 3) {
            GameObject *directionalLight3 = GameObject::CreateDirectionalLight();
            directionalLight3->GetTransform()->setRotation(glm::vec3(0.0f, 0.0f, M_PI_2));
            scene->AddGameObject(directionalLight3);
        }
    }
}
