//
//  RendererTests.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "RendererTests.h"

namespace AKOpenGLEngine {

    void RendererTests::SetUp() {
        Test::SetUp();
        renderer = new Renderer();
        try {
            renderer->CreateDebugContext();
        } catch (rendererException e) {
            ADD_FAILURE();
        }
    }

    void RendererTests::TearDown() {
        Test::TearDown();
        renderer->DestroyWindow();
    }
}