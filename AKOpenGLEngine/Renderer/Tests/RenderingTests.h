//
//  RenderingTests.h
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 26.04.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//


#ifndef __RenderingTests_H_
#define __RenderingTests_H_

#include <string>

#include "RendererTests.h"

namespace AKOpenGLEngine {

    class Scene;
    class Material;
    class Mesh;

    class RenderingTests : public RendererTests {


    protected:
        void InitScene();
        void AddCamera();
        void AddDirectionalLights(int lights);
        Scene *scene;
    };
}


#endif //__RenderingTests_H_
