//
//  main.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 15.01.2015.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include <iostream>

#include "Renderer/Renderer.h"
#include "Renderer/Utilities/WorkingDirectory.h"

int main (int argc, char * argv[]) {
    AKOpenGLEngine::WorkingDirectory::SetArgv0(argv[0]);
    (void) argc;
    (void) argv;
    AKOpenGLEngine::Renderer *renderer = new AKOpenGLEngine::Renderer();
    renderer->InitLogger();
	renderer->CreateOpenGLWindow();
    renderer->Run();

    exit(EXIT_SUCCESS);
}
