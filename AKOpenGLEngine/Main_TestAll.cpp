//
//  Main_TestAll.cpp
//  AKOpenGLEngine
//
//  Created by Adrian Krupa on 09.03.15.
//  Copyright (c) 2015 Adrian Krupa. All rights reserved.
//

#include "gtest/gtest.h"
#include "Renderer/Utilities/WorkingDirectory.h"

GTEST_API_ int main (int argc, char * argv[]) {
    AKOpenGLEngine::WorkingDirectory::SetArgv0(argv[0]);
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}