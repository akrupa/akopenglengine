uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
uname_M := $(shell sh -c 'uname -m 2>/dev/null || echo not')

ifeq ($(uname_S),Darwin)
	DATE=gdate
	LIBDIR = osx
	OPENGL_LINK = -framework OpenGL
else
	DATE=date
	ifneq (,$(findstring arm,$(uname_M)))
		LIBDIR = rpi
	else
		LIBDIR = linux
	endif
	OPENGL_LINK = -lGL
endif

#### PROJECT SETTINGS ####
# The name of the executable to be created
BIN_NAME := AKEngine
# Compiler used
CXX ?= g++
# Extension of source files used in the project
SRC_EXT = cpp
# Path to the source directory, relative to the makefile
SRC_PATH = AKOpenGLEngine/
# Space-separated pkg-config libraries used by this project
LIBS =
# General compiler flags
COMPILE_FLAGS = -std=c++0x -Wall -Wextra -Wno-unknown-pragmas
# Additional release-specific flags
RCOMPILE_FLAGS = -D NDEBUG -Ofast
# Additional debug-specific flags
DCOMPILE_FLAGS = -D _DEBUG -g
# Additional test-specific flags
TCOMPILE_FLAGS = -D _DEBUG -D _TEST -g
# Add additional include paths
INCLUDES = -I $(SRC_PATH)/ -Ilibs/include
# General linker settings
RRPATH = '$ORIGIN/libs'
LINK_FLAGS = -Llibs/lib/$(LIBDIR) -lassimp -lGLEW -lpthread -lglfw -lyaml-cpp -lfreeimage $(OPENGL_LINK) -Wl,-rpath -Wl,$(value RRPATH)
# Additional release-specific linker settings
RLINK_FLAGS =
# Additional debug-specific linker settings
DLINK_FLAGS =
# Additional test-specific linker settings
TLINK_FLAGS =
# Destination directory, like a jail or mounted system
DESTDIR = build/
# Install path (bin/ is appended automatically)
INSTALL_PREFIX = usr/local
#### END PROJECT SETTINGS ####

# Generally should not need to edit below this line

# Shell used in this makefile
# bash is used for 'echo -en'
SHELL = /bin/bash
# Clear built-in rules
.SUFFIXES:
# Programs for installation
INSTALL = install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = $(INSTALL) -m 644

# Append pkg-config specific libraries if need be
ifneq ($(LIBS),)
	COMPILE_FLAGS += $(shell pkg-config --cflags $(LIBS))
	LINK_FLAGS += $(shell pkg-config --libs $(LIBS))
endif

# Verbose option, to output compile and link commands
export V := false
export CMD_PREFIX := @
ifeq ($(V),true)
	CMD_PREFIX :=
endif

# Combine compiler and linker flags
release: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(RCOMPILE_FLAGS)
release: export LDFLAGS := $(LDFLAGS) $(value LINK_FLAGS) $(RLINK_FLAGS)
test: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(TCOMPILE_FLAGS)
test: export LDFLAGS := $(LDFLAGS) $(value LINK_FLAGS) $(TLINK_FLAGS)
debug: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(DCOMPILE_FLAGS)
debug: export LDFLAGS := $(LDFLAGS) $(value LINK_FLAGS) $(DLINK_FLAGS)

test: export EXCLUDED_MAIN_FILE := main.cpp
release: export EXCLUDED_MAIN_FILE := Main_TestAll.cpp
debug: export EXCLUDED_MAIN_FILE := Main_TestAll.cpp

test: export EXCLUDED_TEST_FILE := dumb.cpp
release: export EXCLUDED_TEST_FILE := gtest-all.cpp
debug: export EXCLUDED_TEST_FILE := gtest-all.cpp

test: export EXCLUDED_DIRECTORY :=
release: export EXCLUDED_DIRECTORY := */Tests/*
debug: export EXCLUDED_DIRECTORY := */Tests/*


# Build and output paths
release: export BUILD_PATH := build/release
release: export BIN_PATH := bin/release
test: export BUILD_PATH := build/test
test: export BIN_PATH := bin/test
debug: export BUILD_PATH := build/debug
debug: export BIN_PATH := bin/debug
install: export BIN_PATH := bin/release

# Find all source files in the source directory, sorted by most
# recently modified
SOURCES = $(shell find $(SRC_PATH)/ -name '*.$(SRC_EXT)' ! -iname '$(EXCLUDED_MAIN_FILE)' ! -iname '$(EXCLUDED_TEST_FILE)' -not -path '$(EXCLUDED_DIRECTORY)' | sort -k 1nr | cut -f2-)

SHADERS = $(shell find $(SRC_PATH)/Renderer/ -name '*sh'  | sort -k 1nr | cut -f2-)

# fallback in case the above fails
rwildcard = $(foreach d, $(wildcard $1*), $(call rwildcard,$d/,$2) \
						$(filter $(subst *,%,$2), $d))
ifeq ($(SOURCES),)
	SOURCES := $(call rwildcard, $(SRC_PATH)/, *.$(SRC_EXT))
endif

# Set the object file names, with the source directory stripped
# from the path, and the build path prepended in its place
OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(SRC_EXT)=$(BUILD_PATH)/%.o)
# Set the dependency files that will be used to add header dependencies
DEPS = $(OBJECTS:.o=.d)

# Macros for timing compilation
TIME_FILE = $(dir $@).$(notdir $@)_time
START_TIME = $(DATE) '+%s' > $(TIME_FILE)
END_TIME = read st < $(TIME_FILE) ; \
	$(RM) $(TIME_FILE) ; \
	st=$$((`$(DATE) '+%s'` - $$st - 86400)) ; \
	echo `$(DATE) -u -d @$$st '+%H:%M:%S'`

# Version macros
# Comment/remove this section to remove versioning
USE_VERSION := false
# If this isn't a git repo or the repo has no tags, git describe will return non-zero
ifeq ($(shell git describe > /dev/null 2>&1 ; echo $$?), 0)
	USE_VERSION := true
	VERSION := $(shell git describe --tags --long --dirty --always | \
		sed 's/v\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\)-\?.*-\([0-9]*\)-\(.*\)/\1 \2 \3 \4 \5/g')
	VERSION_MAJOR := $(word 1, $(VERSION))
	VERSION_MINOR := $(word 2, $(VERSION))
	VERSION_PATCH := $(word 3, $(VERSION))
	VERSION_REVISION := $(word 4, $(VERSION))
	VERSION_HASH := $(word 5, $(VERSION))
	VERSION_STRING := \
		"$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH).$(VERSION_REVISION)-$(VERSION_HASH)"
	override CXXFLAGS := $(CXXFLAGS) \
		-D VERSION_MAJOR=$(VERSION_MAJOR) \
		-D VERSION_MINOR=$(VERSION_MINOR) \
		-D VERSION_PATCH=$(VERSION_PATCH) \
		-D VERSION_REVISION=$(VERSION_REVISION) \
		-D VERSION_HASH=\"$(VERSION_HASH)\"
endif

# Debug build for gdb debugging
.PHONY: debug
debug: dirs
ifeq ($(USE_VERSION), true)
	@echo "Beginning debug build v$(VERSION_STRING)"
else
	@echo "Beginning debug build"
endif
	@$(START_TIME)
	@$(MAKE) all --no-print-directory
	@echo -n "Total build time: "
	@$(END_TIME)

# Standard, non-optimized release build
.PHONY: release
release: dirs
ifeq ($(USE_VERSION), true)
	@echo "Beginning release build v$(VERSION_STRING)"
else
	@echo "Beginning release build"
endif
	@$(START_TIME)
	@$(MAKE) all --no-print-directory
	@echo -n "Total build time: "
	@$(END_TIME)

# Test build for gtests
.PHONY: test
test: dirs
ifeq ($(USE_VERSION), true)
	@echo "Beginning test build v$(VERSION_STRING)"
else
	@echo "Beginning test build"
endif
	@$(START_TIME)
	@$(MAKE) all --no-print-directory
	@echo -n "Total build time: "
	@$(END_TIME)

# Create the directories used in the build
.PHONY: dirs
dirs:
	@echo "Creating directories"
	@mkdir -p $(dir $(OBJECTS))
	@mkdir -p $(BIN_PATH)

# Installs to the set path
.PHONY: install
install:
	@echo "Installing to $(DESTDIR)$(INSTALL_PREFIX)/bin"
	@$(INSTALL_PROGRAM) $(BIN_PATH)/$(BIN_NAME) $(DESTDIR)$(INSTALL_PREFIX)/bin

# Uninstalls the program
.PHONY: uninstall
uninstall:
	@echo "Removing $(DESTDIR)$(INSTALL_PREFIX)/bin/$(BIN_NAME)"
	@$(RM) $(DESTDIR)$(INSTALL_PREFIX)/bin/$(BIN_NAME)

# Removes all build files
.PHONY: clean
clean:
	@echo "Deleting $(BIN_NAME) symlink"
	@$(RM) $(BIN_NAME)
	@echo "Deleting directories"
	@$(RM) -r build
	@$(RM) -r bin

# Main rule, checks the executable and symlinks to the output
all: $(BIN_PATH)/$(BIN_NAME)
	@echo "Making symlink: $(BIN_NAME) -> $<"
	@$(RM) $(BIN_NAME)
	@ln -s $(BIN_PATH)/$(BIN_NAME) $(BIN_NAME)
	@mkdir -p $(BIN_PATH)/libs
	@mkdir -p $(BIN_PATH)/shaders
	@cp $(SRC_PATH)/../libs/lib/$(LIBDIR)/* $(BIN_PATH)/libs/
	@cp $(SHADERS) $(BIN_PATH)/shaders/
	@cp -R $(SRC_PATH)/Renderer/resources $(BIN_PATH)

# Link the executable
$(BIN_PATH)/$(BIN_NAME): $(OBJECTS)
	@echo "Linking: $@"
	@$(START_TIME)
	$(CMD_PREFIX)$(CXX) $(OBJECTS) $(LDFLAGS) -o $@
	@echo -en "\t Link time: "
	@$(END_TIME)

# Add dependency files, if they exist
-include $(DEPS)

# Source file rules
# After the first compilation they will be joined with the rules from the
# dependency files to provide header dependencies
$(BUILD_PATH)/%.o: $(SRC_PATH)/%.$(SRC_EXT)
	@echo "Compiling: $< -> $@"
	@$(START_TIME)
	$(CMD_PREFIX)$(CXX) $(CXXFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@
	@echo -en "\t Compile time: "
	@$(END_TIME)
